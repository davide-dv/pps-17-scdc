# PPS-17-SCDC Smart City Devices Coordinator
A coordinator system capable of managing and organizing autonomous systems in a Smart City environment.

# System Requirements
To run the application, it's compulsory to install RabbitMQ on your machine and start the service before opening the application.
Meanwhile, to use all the features it is  necessary to install rabbimqadmin and rabbitmqmanagement.

# Authors
 - Luca Agatensi
 - Elia Bracci
