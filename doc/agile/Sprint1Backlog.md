| Item                | Sprint tasks                        | Volunteer   | Initial Effort | Remaining Effort | 
|---------------------|-------------------------------------|-------------|----------------|------------------|
| Repo Setup          |                                     |             |                |                  | 
|                     | Repository creation                 | Del Vecchio | 1              | 0                | 
|                     | Pipelines integration               | Del Vecchio | 1              | 0                | 
|                     | Trello board creation               | Tagliente   | 1              | 0                | 
| Project setup       |                                     |             |                |                  | 
|                     | Project creation and initialization | Bracci      | 1              | 0                | 
|                     | Gradle configuration                | Bracci      | 1              | 0                | 
|                     | GitFlow configuration               | Agatensi    | 1              | 0                | 
| Agile investigation |                                     |             |                |                  | 
|                     | Agile process study                 | Agatensi    | 1              | 0                | 
|                     | Scrum documentation study           | Tagliente   | 1              | 0                |

    
              


    
              
