| Item                | Sprint tasks                        | Volunteer(s)  | Initial Effort | Remaining Effort | 
|---------------------|-------------------------------------|-------------|----------------|------------------|
|  System distributed architecture defintion         |  Distributed technologies implementation                                    | Bracci, Tagliente         |   20             | 0                 | 
|  Components behaviours and interaction implementation         |  Analysis and implementation of behaviours                                   |   Agatensi, Del Vecchio         |   50             | 0                 | 
|As user, I want to select the scenario          | Menu creation | Bracci | 20 | 0 
| As user, I want to interact with the demo        | Buttons creation    | Bracci |20           | 0              |
|As user, I want to see a demo of the scenarios         | Scenario creation | Bracci | 50 | 0
|  | Connection between Controller and View | Bracci, Del Vecchio | 20          | 0 |



    
              
