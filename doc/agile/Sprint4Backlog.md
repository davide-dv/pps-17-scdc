| Item                | Sprint tasks                        | Volunteer(s)  | Initial Effort | Remaining Effort | 
|---------------------|-------------------------------------|-------------|----------------|------------------|
|  System distributed architecture defintion         |  Research about distributed technologies                                   | Tagliente         |   50             | 0                 | 
|          |  Distributed technologies implementation                                    | Bracci         |   50             | 20                 | 
|  Components behaviours and interaction implementation         |  Analysis and implementation of behaviours                                   |   Agatensi, Del Vecchio         |   100             | 50                 | 
        


    
              
