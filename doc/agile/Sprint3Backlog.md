| Item                | Sprint tasks                        | Volunteer(s)  | Initial Effort | Remaining Effort | 
|---------------------|-------------------------------------|-------------|----------------|------------------|
|  Basic system components definition and implementation          |                                     |             |                |                  | 
|                     | Definition of main system components                 | Tagliente | 10   |                0| 
|  View Implementation        |                                     |    Bracci         |   10             | 0                 | 
|  Controller Implementation          |                                     | Del Vecchio         |   10             | 0                 | 
|  Model Implementation          |                                     | Agatensi         |10                |  0                | 
         


    
              
