| Priority | Type          | Item                                                  | Story Points | Estimated Hours | 
|----------|---------------|-------------------------------------------------------|--------------|-----------------| 
| 1        | Setup         | Repository Setup                                      | 1            | 1               | 
| 2        | Setup         | Project Setup                                         | 3            | 2               | 
| 3        | Investigation | Agile techniques investigation                        | 2            | 4               | 
| 4        | Epic          | Basic system components definition and implementation | 100          | 40              | 
| 5        | Epic          | Components behaviours and interaction implementation  | 100          | 40              | 
| 6        | Epic          | System distributed architecture defintion             | 40           | 32              | 
| 7        | Story         | As user, I want to select the scenario                | 20           | 20              | 
| 8        | Story         | As user, I want to see a demo of the scenarios        | 100          | 32              | 
| 9        | Story         | As user, I want to interact with the demo             | 20           | 12              | 
| 10       | Epic          | Control System implementation                         | 13           | 24              |