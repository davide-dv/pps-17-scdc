#!/bin/bash

UNAME=$(uname)

if [[ "$UNAME" == "Linux" ]] ; then
    python3 rabbitmqadmin -f tsv -q list queues name > queues.txt
    while read -r name; do python3 rabbitmqadmin -q delete queue name="${name}"; done < queues.txt
elif [[ "$UNAME" == CYGWIN* || "$UNAME" == MINGW* ]] ; then
	str=""
    python.exe rabbitmqadmin -f tsv -q list queues name > queues.txt
    while read -r name; do str="${name::-1}"; python.exe rabbitmqadmin -q delete queue name="${str}"; done < queues.txt
fi