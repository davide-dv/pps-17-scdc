package view;

import controller.basic.Controller;
import controller.basic.Controller$;
import controller.basic.NewElementInit;
import controller.basic.Scenario;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.components.actuators.ColorEnum;
import model.components.actuators.Semaphore;
import model.components.basic.*;
import scala.Function1;
import scala.Option;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import view.controllersutilities.ScenarioControllerUtilities;

import java.util.List;

/** Class for Cross Road Scenario */
@SuppressWarnings("unchecked")
public class CrossRoadController implements ScenarioController {

  @FXML public AnchorPane mainPane;

  public Button startButton;

  public Button pauseButton;

  private boolean isRunning = false;

  private NewElementInit newElementInit;

  @FXML private GridPane worldGrid;

  private Controller crossRoadController;

  private static final int NUM_ROWS = 8;

  private static final int NUM_COLUMNS = 8;

  @Override
  public Button getStartButton() {
    return this.startButton;
  }

  @Override
  public Button getPauseButton() {
    return this.pauseButton;
  }

  @Override
  public void setRunning(boolean isRunning) {
    this.isRunning = isRunning;
  }

  @Override
  public void startController() {
    this.crossRoadController.startSimulation();
  }

  @Override
  public void startSimulation() {
    ScenarioControllerUtilities.startSimulation(this);
  }

  @Override
  public void pauseSimulation() {
    ScenarioControllerUtilities.pauseSimulation(this);
  }

  @Override
  public List getList(scala.collection.immutable.List list) {
    return ScenarioControllerUtilities.getList(list);
  }

  @Override
  public Node getNodeByID(final Pane pane, final String id) {
    return ScenarioControllerUtilities.getNodeByID(pane, id);
  }

  /**
   * Method that draws street on the stage
   *
   * @param style css style information
   * @param cellList list of element to be drawn
   */
  private void drawStreet(final String style, final List<Cell> cellList) {
    cellList.forEach(
        cell -> {
          Node currentNode =
              ScenarioControllerUtilities.getNodeByRowColumnIndex(
                  cell.column(), cell.row(), this.worldGrid);
          if (cell.row() == (NUM_ROWS / 2) - 1 && cell.column() == (NUM_COLUMNS / 2) - 1) {
            currentNode.setStyle(
                "-fx-background-color: darkgrey; -fx-border-color: white; -fx-border-width: 5px 0px 0px 0px");
          } else if (cell.row() == (NUM_ROWS / 2) - 1 && cell.column() == (NUM_COLUMNS / 2)) {
            currentNode.setStyle(
                "-fx-background-color: darkgrey; -fx-border-color: white; -fx-border-width: 0px 5px 0px 0px");
          } else if (cell.row() == (NUM_ROWS / 2) && cell.column() == (NUM_COLUMNS / 2) - 1) {
            currentNode.setStyle(
                "-fx-background-color: darkgrey; -fx-border-color: white; -fx-border-width: 0px 0px 0px 5px");
          } else if (cell.row() == (NUM_ROWS / 2) && cell.column() == (NUM_ROWS / 2)) {
            currentNode.setStyle(
                "-fx-background-color: darkgrey; -fx-border-color: white; -fx-border-width: 0px 0px 5px 0px");
          } else {
            currentNode.setStyle(style);
          }
        });
  }

  /**
   * Method that draws a vehicle on the stage
   *
   * @param vehicle the vehicle that has to be drawn
   * @param rotate rotation used for direction
   */
  private void drawVehicle(final Vehicle vehicle, final int rotate) {
    if (this.getNodeByID(this.mainPane, vehicle.id()) == null) {
      ImageView image = new ImageView("/view/images/car.png");
      image.setRotate(image.getRotate() + rotate);
      HBox hbox = new HBox(image);
      hbox.setLayoutX(vehicle.position().column() * 75);
      hbox.setLayoutY(vehicle.position().row() * 75);
      hbox.setId(vehicle.id());

      this.mainPane.getChildren().add(hbox);
    } else {
      this.moveVehicle(vehicle);
    }
  }

  /**
   * Method to update the view
   *
   * @param elementList list of basic element updated
   */
  private void update(final List<BasicElement> elementList) {
    Platform.runLater(
        (() ->
            elementList.forEach(
                element -> {
                  if (element instanceof Vehicle) {
                    this.checkVehicleDirection((Vehicle) element);
                  } else if (element instanceof Semaphore) {
                    Semaphore semaphore = (Semaphore) element;
                    this.checkSemaphoreDirection(semaphore, true);
                  }
                })));
    if (ScenarioControllerUtilities.getRandomBoolean())
      this.newElementInit.initNewVehicle(elementList);
    ScenarioControllerUtilities.scheduleTimerTask(isRunning, this, 1500L);
  }

  /**
   * Method to move a vehicle
   *
   * @param v the vehicle to be moved
   */
  private void moveVehicle(final Vehicle v) {
    Node nodeVehicle = this.getNodeByID(this.mainPane, v.id());
    if (v.direction().equals(DirectionEnum.SOUTH()) && v.position().row() > 7) {
      nodeVehicle.setLayoutY(v.position().row() * 75);
    }
    if (v.direction().equals(DirectionEnum.SOUTH())
        || v.direction().equals(DirectionEnum.NORTH())) {
      ScenarioControllerUtilities.setTransition(
          2.2,
          "y",
          v.position().row(),
          (v.position().row() - nodeVehicle.getLayoutY() / 75) * 75,
          nodeVehicle);
    } else {
      ScenarioControllerUtilities.setTransition(
          2.2,
          "x",
          v.position().column(),
          (v.position().column() - nodeVehicle.getLayoutX() / 75) * 75,
          nodeVehicle);
    }
  }

  /**
   * Method that draws a vehicle checking the direction
   *
   * @param vehicle the vehicle to be drawn
   */
  private void checkVehicleDirection(final Vehicle vehicle) {
    if (vehicle.direction().equals(DirectionEnum.NORTH())) {
      this.drawVehicle(vehicle, -90);
    } else if (vehicle.direction().equals(DirectionEnum.EAST())) {
      this.drawVehicle(vehicle, 0);
    } else if (vehicle.direction().equals(DirectionEnum.WEST())) {
      this.drawVehicle(vehicle, -180);
    } else if (vehicle.direction().equals(DirectionEnum.SOUTH())) {
      this.drawVehicle(vehicle, 90);
    }
  }

  /**
   * Method for drawing the street considering the direction
   *
   * @param s the street to be drawn
   */
  private void checkStreetDirection(final StreetElement s) {
    if (s.direction().equals(DirectionEnum.NORTH())) {
      this.drawStreet(
          "-fx-background-color: darkgrey; -fx-border-color: white; -fx-border-width: 0px "
              + "0px 0px 1px",
          this.getList(s.cellList()));
    } else if (s.direction().equals(DirectionEnum.SOUTH())) {
      this.drawStreet(
          "-fx-background-color: darkgrey; -fx-border-color: white; -fx-border-width: 0px "
              + "1px 0px 0px",
          this.getList(s.cellList()));
    } else if (s.direction().equals(DirectionEnum.EAST())) {
      this.drawStreet(
          "-fx-background-color: darkgrey; -fx-border-color: white; "
              + "-fx-border-width: 1px "
              + "0px 0px 0px",
          this.getList(s.cellList()));
    } else if (s.direction().equals(DirectionEnum.WEST())) {
      this.drawStreet(
          "-fx-background-color: darkgrey; -fx-border-color: white; -fx-border-width: 0px "
              + "0px 1px 0px",
          this.getList(s.cellList()));
    }
  }

  /**
   * Method for drawing semaphores
   *
   * @param id the semaphore id
   * @param col the semaphore column
   * @param row the semaphore row
   * @param rotate the semaphore rotation
   * @param state the semaphore state
   */
  private void drawSemaphore(
      final String id, final int col, final int row, final int rotate, final String state) {
    Node semaphoreNode =
        ScenarioControllerUtilities.getNodeByRowColumnIndex(col, row, this.worldGrid);
    ImageView semaphoreImage = getSemaphoreImage(rotate, state);
    HBox hBox = new HBox(semaphoreImage);

    semaphoreNode.setStyle("-fx-background-color: green;");
    hBox.setId(id);
    ((AnchorPane) semaphoreNode).getChildren().add(hBox);
  }

  /**
   * Method for updating semaphore state in the stage
   *
   * @param id the semaphore id
   * @param rotate the semaphore rotation
   * @param state the semaphore state
   */
  private void updateSemaphore(final String id, final int rotate, final String state) {
    HBox semaphoreNode = (HBox) this.getNodeByID(this.worldGrid, id);
    ImageView semaphoreImage = getSemaphoreImage(rotate, state);
    ((ImageView) semaphoreNode.getChildren().get(0)).setImage(semaphoreImage.getImage());
  }

  /**
   * Getter for semaphore image
   *
   * @param rotate the semaphore rotation
   * @param state the semaphore state
   * @return the semaphore image
   */
  private ImageView getSemaphoreImage(int rotate, String state) {
    ImageView semaphoreImage = new ImageView();
    if (state.equals(ColorEnum.RED())) {
      semaphoreImage = new ImageView("/view/images/traffic-light-red.png");
    } else if (state.equals(ColorEnum.GREEN())) {
      semaphoreImage = new ImageView("/view/images/traffic-light-green.png");
    } else if (state.equals(ColorEnum.YELLOW())) {
      semaphoreImage = new ImageView("/view/images/traffic-light-yellow.png");
    }
    semaphoreImage.setRotate(semaphoreImage.getRotate() + rotate);
    return semaphoreImage;
  }

  /**
   * Method for drawing semaphore, checking its direction
   *
   * @param semaphore the semaphore to be drawn
   * @param update true if semaphore has to be updated, false for drawing it
   */
  private void checkSemaphoreDirection(final Semaphore semaphore, boolean update) {
    if (semaphore.direction().equals(DirectionEnum.NORTH())) {
      if (update) {
        this.updateSemaphore(semaphore.id(), 0, semaphore.semaphoreInformation().state());
      } else {
        this.drawSemaphore(
            semaphore.id(),
            semaphore.position().column() + 1,
            semaphore.position().row(),
            0,
            semaphore.semaphoreInformation().state());
      }
    } else if (semaphore.direction().equals(DirectionEnum.EAST())) {
      if (update) {
        this.updateSemaphore(semaphore.id(), 90, semaphore.semaphoreInformation().state());
      } else {
        this.drawSemaphore(
            semaphore.id(),
            semaphore.position().column(),
            semaphore.position().row() + 1,
            90,
            semaphore.semaphoreInformation().state());
      }
    } else if (semaphore.direction().equals(DirectionEnum.WEST())) {
      if (update) {
        this.updateSemaphore(semaphore.id(), -90, semaphore.semaphoreInformation().state());
      } else {
        this.drawSemaphore(
            semaphore.id(),
            semaphore.position().column(),
            semaphore.position().row() - 1,
            -90,
            semaphore.semaphoreInformation().state());
      }
    } else if (semaphore.direction().equals(DirectionEnum.SOUTH())) {
      if (update) {
        this.updateSemaphore(semaphore.id(), 180, semaphore.semaphoreInformation().state());
      } else {
        this.drawSemaphore(
            semaphore.id(),
            semaphore.position().column() - 1,
            semaphore.position().row(),
            180,
            semaphore.semaphoreInformation().state());
      }
    }
  }

  /**
   * Method for drawing all the scenario
   *
   * @param elements the list of basic element to be drawn
   */
  private void drawCrossRoad(final List<BasicElement> elements) {
    elements.forEach(
        element -> {
          if (element instanceof Vehicle) {
            this.checkVehicleDirection((Vehicle) element);
          } else if (element instanceof StreetElement) {
            StreetElement s = (StreetElement) element;
            this.checkStreetDirection(s);
          } else if (element instanceof Semaphore) {
            Semaphore semaphore = (Semaphore) element;
            this.checkSemaphoreDirection(semaphore, false);
          }
        });
  }

  /** Initialization of the scenario and the controller */
  private void initScenario() {
    CrossRoadSettingsController crossRoadSettingsController = new CrossRoadSettingsController();
    this.newElementInit = new NewElementInit();
    this.startButton.setDefaultButton(true);
    this.pauseButton.setDefaultButton(false);
    this.pauseButton.setDisable(true);
    this.crossRoadController =
        Controller$.MODULE$.apply(
            new Tuple2<>(CrossRoadController.NUM_COLUMNS, CrossRoadController.NUM_ROWS),
            Scenario.CROSS_ROAD(),
            Option.apply(crossRoadSettingsController.getSettings()));
    Function1<Controller, BoxedUnit> f =
        new AbstractFunction1<Controller, BoxedUnit>() {
          @Override
          public BoxedUnit apply(Controller controller) {
            update(getList(controller.elements()));
            return null;
          }
        };
    this.crossRoadController.addObserver(f);

    for (int i = 0; i < CrossRoadController.NUM_COLUMNS; i++) {
      for (int j = 0; j < CrossRoadController.NUM_ROWS; j++) {
        this.worldGrid.add(new AnchorPane(), i, j);
      }
    }

    this.worldGrid.getChildren().forEach(node -> node.setStyle("-fx-background-color: green;"));

    this.worldGrid.setAlignment(Pos.CENTER);
  }

  @FXML
  void initialize() {
    assert worldGrid != null
        : "fx:id=\"worldGrid\" was not injected: check your FXML file 'CrossRoad.fxml'.";

    this.initScenario();
    this.drawCrossRoad(this.getList(this.crossRoadController.elements()));
  }
}
