package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import model.components.actuators.ColorEnum;
import view.controllersutilities.CrossRoadSettings;
import view.controllersutilities.FileUtilities;

import java.io.InputStream;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class CrossRoadSettingsController {

    public static final String CROSS_ROAD_SETTINGS_FILE_PATH =
            CrossRoadSettingsController.class.getResourceAsStream("/bashscript/cross_road_settings.conf").toString();
    public static final InputStream DELETE_ALL_QUEUES_SCRIPT =
            CrossRoadSettingsController.class.getResourceAsStream("/bashscript/deleteAllQueues.sh");
    public static final InputStream RABBITMQ_ADMIN_SCRIPT =
            CrossRoadSettingsController.class.getResourceAsStream("/bashscript/rabbitmqadmin");


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="southVehiclesChoiceBox"
    private ChoiceBox<Integer> southVehiclesChoiceBox; // Value injected by FXMLLoader

    @FXML // fx:id="eastVehiclesChoiceBox"
    private ChoiceBox<Integer> eastVehiclesChoiceBox; // Value injected by FXMLLoader

    @FXML // fx:id="saveAndStartButton"
    private Button saveAndExitButton; // Value injected by FXMLLoader

    @FXML // fx:id="southSemaphoreChoiceBox"
    private ChoiceBox<String> southSemaphoreChoiceBox; // Value injected by FXMLLoader

    @FXML // fx:id="northVehiclesChoiceBox"
    private ChoiceBox<Integer> northVehiclesChoiceBox; // Value injected by FXMLLoader

    @FXML // fx:id="eastSemaphoreChoiceBox"
    private ChoiceBox<String> eastSemaphoreChoiceBox; // Value injected by FXMLLoader

    @FXML // fx:id="northSemaphoreChoiceBox"
    private ChoiceBox<String> northSemaphoreChoiceBox; // Value injected by FXMLLoader

    @FXML // fx:id="westVehiclesChoiceBox"
    private ChoiceBox<Integer> westVehiclesChoiceBox; // Value injected by FXMLLoader

    @FXML // fx:id="westSemaphoreChoiceBox"
    private ChoiceBox<String> westSemaphoreChoiceBox; // Value injected by FXMLLoader

    private CrossRoadSettings crossRoadSettings;

    @FXML
        // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert southVehiclesChoiceBox != null
                : "fx:id=\"southVehiclesChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert eastVehiclesChoiceBox != null
                : "fx:id=\"eastVehiclesChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert saveAndExitButton != null
                : "fx:id=\"saveAndStartButton\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert southSemaphoreChoiceBox != null
                : "fx:id=\"southSemaphoreChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert northVehiclesChoiceBox != null
                : "fx:id=\"northVehiclesChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert eastSemaphoreChoiceBox != null
                : "fx:id=\"eastSemaphoreChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert northSemaphoreChoiceBox != null
                : "fx:id=\"northSemaphoreChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert westVehiclesChoiceBox != null
                : "fx:id=\"westVehiclesChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";
        assert westSemaphoreChoiceBox != null
                : "fx:id=\"westSemaphoreChoiceBox\" was not injected: check your FXML file 'CrossRoadSettings.fxml'.";

        ObservableList<Integer> numbers = FXCollections.observableArrayList(1, 2, 3);

        this.crossRoadSettings =
                (CrossRoadSettings) FileUtilities.readFromFile(CROSS_ROAD_SETTINGS_FILE_PATH);

        this.northVehiclesChoiceBox.setItems(numbers);

        this.eastVehiclesChoiceBox.setItems(numbers);

        this.southVehiclesChoiceBox.setItems(numbers);

        this.westVehiclesChoiceBox.setItems(numbers);

        ObservableList<String> semaphoresStates =
                FXCollections.observableArrayList(ColorEnum.RED(), ColorEnum.YELLOW(), ColorEnum.GREEN());

        this.northSemaphoreChoiceBox.setItems(semaphoresStates);

        this.eastSemaphoreChoiceBox.setItems(semaphoresStates);

        this.southSemaphoreChoiceBox.setItems(semaphoresStates);

        this.westSemaphoreChoiceBox.setItems(semaphoresStates);

        if (this.crossRoadSettings == null) {
            this.northVehiclesChoiceBox.setValue(numbers.get(0));
            this.eastVehiclesChoiceBox.setValue(numbers.get(0));
            this.southVehiclesChoiceBox.setValue(numbers.get(0));
            this.westVehiclesChoiceBox.setValue(numbers.get(0));
            this.northSemaphoreChoiceBox.setValue(semaphoresStates.get(0));
            this.eastSemaphoreChoiceBox.setValue(semaphoresStates.get(0));
            this.southSemaphoreChoiceBox.setValue(semaphoresStates.get(0));
            this.westSemaphoreChoiceBox.setValue(semaphoresStates.get(0));
        } else {
            this.northVehiclesChoiceBox.setValue(this.crossRoadSettings.getNorthVehicles());
            this.eastVehiclesChoiceBox.setValue(this.crossRoadSettings.getEastVehicles());
            this.southVehiclesChoiceBox.setValue(this.crossRoadSettings.getSouthVehicles());
            this.westVehiclesChoiceBox.setValue(this.crossRoadSettings.getWestVehicles());
            this.northSemaphoreChoiceBox.setValue(this.crossRoadSettings.getNorthSemaphoreState());
            this.eastSemaphoreChoiceBox.setValue(this.crossRoadSettings.getEastSemaphoreState());
            this.southSemaphoreChoiceBox.setValue(this.crossRoadSettings.getSouthSemaphoreState());
            this.westSemaphoreChoiceBox.setValue(this.crossRoadSettings.getWestSemaphoreState());
        }
    }

    public void saveSettingsAndExit(ActionEvent actionEvent) {
        if (this.crossRoadSettings == null) {
            this.crossRoadSettings =
                    new CrossRoadSettings(
                            this.northVehiclesChoiceBox.getValue(),
                            this.eastVehiclesChoiceBox.getValue(),
                            this.southVehiclesChoiceBox.getValue(),
                            this.westVehiclesChoiceBox.getValue(),
                            this.northSemaphoreChoiceBox.getValue(),
                            this.eastSemaphoreChoiceBox.getValue(),
                            this.southSemaphoreChoiceBox.getValue(),
                            this.westSemaphoreChoiceBox.getValue());
        } else {
            this.crossRoadSettings.setNorthVehicles(this.northVehiclesChoiceBox.getValue());
            this.crossRoadSettings.setEastVehicles(this.eastVehiclesChoiceBox.getValue());
            this.crossRoadSettings.setSouthVehicles(this.southVehiclesChoiceBox.getValue());
            this.crossRoadSettings.setWestVehicles(this.westVehiclesChoiceBox.getValue());

            this.crossRoadSettings.setNorthSemaphoreState(this.northSemaphoreChoiceBox.getValue());
            this.crossRoadSettings.setEastSemaphoreState(this.eastSemaphoreChoiceBox.getValue());
            this.crossRoadSettings.setSouthSemaphoreState(this.southSemaphoreChoiceBox.getValue());
            this.crossRoadSettings.setWestSemaphoreState(this.westSemaphoreChoiceBox.getValue());
        }

        FileUtilities.writeToFile(CROSS_ROAD_SETTINGS_FILE_PATH, this.crossRoadSettings);

        Stage stage = (Stage) this.saveAndExitButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    public CrossRoadSettings getSettings() {
        this.crossRoadSettings =
                (CrossRoadSettings) FileUtilities.readFromFile(CROSS_ROAD_SETTINGS_FILE_PATH);
        if (this.crossRoadSettings == null) {
            Random ran = new Random();
            this.crossRoadSettings =
                    new CrossRoadSettings(
                            ran.nextInt(4),
                            ran.nextInt(4),
                            ran.nextInt(4),
                            ran.nextInt(4),
                            ColorEnum.RED(),
                            ColorEnum.RED(),
                            ColorEnum.RED(),
                            ColorEnum.RED());
            return this.crossRoadSettings;
        } else {
            return this.crossRoadSettings;
        }
    }
}
