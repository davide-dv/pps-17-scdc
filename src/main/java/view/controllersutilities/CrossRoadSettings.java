package view.controllersutilities;

import java.io.Serializable;

public class CrossRoadSettings implements Serializable {

  private int eastVehicles;
  private int northVehicles;
  private int southVehicles;
  private int westVehicles;

  private String eastSemaphoreState;
  private String northSemaphoreState;
  private String westSemaphoreState;
  private String southSemaphoreState;

  public CrossRoadSettings(
      int northVehicles,
      int eastVehicles,
      int southVehicles,
      int westVehicles,
      String northSemaphoreState,
      String eastSemaphoreState,
      String southSemaphoreState,
      String westSemaphoreState) {
    this.eastVehicles = eastVehicles;
    this.northVehicles = northVehicles;
    this.southVehicles = southVehicles;
    this.westVehicles = westVehicles;
    this.eastSemaphoreState = eastSemaphoreState;
    this.northSemaphoreState = northSemaphoreState;
    this.westSemaphoreState = westSemaphoreState;
    this.southSemaphoreState = southSemaphoreState;
  }

  public int getEastVehicles() {
    return eastVehicles;
  }

  public void setEastVehicles(int eastVehicles) {
    this.eastVehicles = eastVehicles;
  }

  public int getNorthVehicles() {
    return northVehicles;
  }

  public void setNorthVehicles(int northVehicles) {
    this.northVehicles = northVehicles;
  }

  public int getSouthVehicles() {
    return southVehicles;
  }

  public void setSouthVehicles(int southVehicles) {
    this.southVehicles = southVehicles;
  }

  public int getWestVehicles() {
    return westVehicles;
  }

  public void setWestVehicles(int westVehicles) {
    this.westVehicles = westVehicles;
  }

  public String getEastSemaphoreState() {
    return eastSemaphoreState;
  }

  public void setEastSemaphoreState(String eastSemaphoreState) {
    this.eastSemaphoreState = eastSemaphoreState;
  }

  public String getNorthSemaphoreState() {
    return northSemaphoreState;
  }

  public void setNorthSemaphoreState(String northSemaphoreState) {
    this.northSemaphoreState = northSemaphoreState;
  }

  public String getWestSemaphoreState() {
    return westSemaphoreState;
  }

  public void setWestSemaphoreState(String westSemaphoreState) {
    this.westSemaphoreState = westSemaphoreState;
  }

  public String getSouthSemaphoreState() {
    return southSemaphoreState;
  }

  public void setSouthSemaphoreState(String southSemaphoreState) {
    this.southSemaphoreState = southSemaphoreState;
  }

  @Override
  public String toString() {
    return "CrossRoadSettings{"
        + "eastVehicles="
        + eastVehicles
        + ", northVehicles="
        + northVehicles
        + ", "
        + "southVehicles="
        + southVehicles
        + ", westVehicles="
        + westVehicles
        + ", eastSemaphoreState='"
        + eastSemaphoreState
        + '\''
        + ", northSemaphoreState='"
        + northSemaphoreState
        + '\''
        + ", westSemaphoreState='"
        + westSemaphoreState
        + '\''
        + ", southSemaphoreState='"
        + southSemaphoreState
        + '\''
        + '}';
  }
}
