package view.controllersutilities;

import view.CrossRoadSettingsController;

import java.io.*;

public class FileUtilities {

    public static void writeToFile(String fileName, CrossRoadSettings crossRoadSettings) {
        File f = new File(fileName);
        System.out.println(f);
        try {
            f.createNewFile();
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
            oos.writeObject(crossRoadSettings);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("File Not Found!");
        }
    }

    public static Object readFromFile(String fileName) {
        File f = new File(fileName);
        Object ret = null;
        ObjectInputStream oos;
        try {
            oos = new ObjectInputStream(new FileInputStream(f));
            ret = oos.readObject();
            oos.close();
        } catch (IOException | ClassNotFoundException e) {
            return ret;
        }
        return ret;
    }

    /**
     * Method to delete all RabbitMQ queues
     */
    public static void deleteAllQueues() {
        OutputStream os = null;
        try {
            File file = new File("temp.sh");
            os = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = CrossRoadSettingsController.DELETE_ALL_QUEUES_SCRIPT.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            file.setExecutable(true);

            File fileRabbit = new File("rabbitmqadmin");
            os = new FileOutputStream(fileRabbit);
            byte[] rabbitBuffer = new byte[1024];
            int rabbitBytesRead;
            while ((rabbitBytesRead = CrossRoadSettingsController.RABBITMQ_ADMIN_SCRIPT.read(rabbitBuffer)) != -1) {
                os.write(rabbitBuffer, 0, rabbitBytesRead);
            }
            os.close();
            fileRabbit.setExecutable(true);


            Process process = Runtime.getRuntime().exec(file.getAbsolutePath());
            process.waitFor();
            file.delete();
            fileRabbit.delete();
            File queuesFile = new File("queues.txt");
            queuesFile.delete();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to delete CrossRoad Settings conf
     */
    public static void deleteCrossRoadConf() {
        File file = new File(CrossRoadSettingsController.CROSS_ROAD_SETTINGS_FILE_PATH);
        file.delete();
    }
}
