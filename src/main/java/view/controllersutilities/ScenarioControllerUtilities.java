package view.controllersutilities;

import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import scala.collection.JavaConversions;
import view.ScenarioController;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/** Utils class for view */
@SuppressWarnings({"deprecation", "unchecked"})
public class ScenarioControllerUtilities {

  /**
   * Getter for random boolean used to createor not random person on the stage
   *
   * @return true if the person is drawn, false not
   */
  public static boolean getRandomBoolean() {
    return Math.random() < 0.5;
  }

  /**
   * Method for starting simulation and controller
   *
   * @param scenario the scenario to be started
   */
  public static void startSimulation(final ScenarioController scenario) {
    scenario.getPauseButton().setDisable(false);
    scenario.getStartButton().setDisable(true);
    scenario.getPauseButton().setDefaultButton(true);
    scenario.getStartButton().setDefaultButton(false);
    scenario.setRunning(true);
    scenario.startController();
  }

  /**
   * Method for pausing simulation and controller
   *
   * @param scenario the scenario to be paused
   */
  public static void pauseSimulation(final ScenarioController scenario) {
    scenario.getStartButton().setDisable(false);
    scenario.getPauseButton().setDisable(true);
    scenario.getStartButton().setDefaultButton(true);
    scenario.getPauseButton().setDefaultButton(false);
    scenario.setRunning(false);
  }

  /**
   * Getter for node of the stage with specific characteristics
   *
   * @param column node column
   * @param row node row
   * @param gridPane grid pane to get the node
   * @return the node interested
   */
  public static Node getNodeByRowColumnIndex(
      final int column, final int row, final GridPane gridPane) {
    Node result = null;
    ObservableList<Node> childrens = gridPane.getChildren();
    for (Node node : childrens) {
      if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
        result = node;
        break;
      }
    }
    return result;
  }

  /**
   * Set a transition on the stage
   *
   * @param duration the duration of the transition
   * @param s can be 'x' for transition on x axis or y otherwise
   * @param from initial position of the transition
   * @param to final position of the transition
   * @param node th node interested in the transition
   */
  public static void setTransition(
      final double duration, final String s, final double from, final double to, final Node node) {
    TranslateTransition translateTransition =
        new TranslateTransition(Duration.seconds(duration), node);
    switch (s) {
      case "x":
        translateTransition.setByX(from);
        translateTransition.setToX(to);
        break;
      case "y":
        translateTransition.setByY(from);
        translateTransition.setToY(to);
        break;
    }

    translateTransition.setInterpolator(Interpolator.LINEAR);
    translateTransition.play();
  }

  /**
   * Method for creating a task that starts the controller every time set
   *
   * @param isRunning true if the application is running, false if is on pause
   * @param controller the specific controller of the scenario
   * @param time waiting time
   */
  public static void scheduleTimerTask(
      boolean isRunning, ScenarioController controller, final Long time) {
    Timer timer = new Timer();
    timer.schedule(
        new TimerTask() {
          @Override
          public void run() {
            if (isRunning) controller.startController();
            timer.cancel();
          }
        },
        time);
  }

  /**
   * Getter for convertion from scala list to java list
   *
   * @param list scala list to be converted
   * @return the java list converted
   */
  public static List getList(scala.collection.immutable.List list) {
    return JavaConversions.seqAsJavaList(list);
  }

  /**
   * Getter for node by id
   *
   * @param pane the pane on the stage
   * @param id the id interested
   * @return the node
   */
  public static Node getNodeByID(final Pane pane, final String id) {
    return pane.lookup("#" + id);
  }
}
