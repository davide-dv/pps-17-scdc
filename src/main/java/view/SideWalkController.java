package view;

import controller.basic.Controller;
import controller.basic.Controller$;
import controller.basic.NewElementInit;
import controller.basic.Scenario;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.components.actuators.StreetLamp;
import model.components.actuators.StreetLampStates;
import model.components.basic.*;
import scala.Function1;
import scala.Option;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import view.controllersutilities.ScenarioControllerUtilities;

import java.util.List;

/** Class for Side Walk Scenario */
@SuppressWarnings({"unchecked"})
public class SideWalkController implements ScenarioController {

  public Button startButton;

  public Button pauseButton;

  private Controller sideWalkController;

  private static final int NUM_COLUMNS = 8;

  private static final int NUM_ROWS = 3;

  private boolean isRunning = false;

  private NewElementInit newElementInit;

  @FXML private AnchorPane mainPane;

  @FXML private GridPane worldGrid;

  @Override
  public Button getStartButton() {
    return this.startButton;
  }

  @Override
  public Button getPauseButton() {
    return this.pauseButton;
  }

  @Override
  public void setRunning(boolean isRunning) {
    this.isRunning = isRunning;
  }

  @Override
  public void startController() {
    this.sideWalkController.startSimulation();
  }

  @Override
  public void startSimulation() {
    ScenarioControllerUtilities.startSimulation(this);
  }

  @Override
  public void pauseSimulation() {
    ScenarioControllerUtilities.pauseSimulation(this);
  }

  @Override
  public List getList(scala.collection.immutable.List list) {
    return ScenarioControllerUtilities.getList(list);
  }

  @Override
  public Node getNodeByID(final Pane pane, final String id) {
    return ScenarioControllerUtilities.getNodeByID(pane, id);
  }

  /**
   * Method to update the view
   *
   * @param elementList list of basic element updated
   */
  private void update(List<BasicElement> elementList) {
    Platform.runLater(
        (() ->
            elementList.forEach(
                element -> {
                  if (element instanceof Person) {
                    this.drawPerson((Person) element);
                  } else if (element instanceof StreetLamp) {
                    this.updateStreetLamp(element.id(), ((StreetLamp) element).state());
                  }
                })));
    if (ScenarioControllerUtilities.getRandomBoolean()) {
      this.newElementInit.initNewPerson(ScenarioControllerUtilities.getRandomBoolean());
    }
    ScenarioControllerUtilities.scheduleTimerTask(this.isRunning, this, 1000L);
  }

  /**
   * Method for updating street lamp state in the stage
   *
   * @param id the street lamp id
   * @param state the street lamp state
   */
  private void updateStreetLamp(final String id, final String state) {
    HBox semaphoreNode = (HBox) this.getNodeByID(this.worldGrid, id);
    ImageView semaphoreImage = getStreetLampImage(state);
    ((ImageView) semaphoreNode.getChildren().get(0)).setImage(semaphoreImage.getImage());
  }

  /**
   * Method to move a person
   *
   * @param person the person to be moved
   */
  private void movePerson(Person person) {
    Node nodePerson = this.getNodeByID(this.mainPane, person.id());
    ScenarioControllerUtilities.setTransition(
        2.2,
        "x",
        person.position().column(),
        (person.position().column() - nodePerson.getLayoutX() / 100) * 100,
        nodePerson);
  }

  /** Initialization of the scenario and the controller */
  private void initScenario() {
    this.newElementInit = new NewElementInit();
    this.startButton.setDefaultButton(true);
    this.pauseButton.setDefaultButton(false);
    this.pauseButton.setDisable(true);

    this.sideWalkController =
        Controller$.MODULE$.apply(
            new Tuple2<>(SideWalkController.NUM_COLUMNS, SideWalkController.NUM_ROWS),
            Scenario.SIDE_WALK(),
            Option.empty());

    Function1<Controller, BoxedUnit> f =
        new AbstractFunction1<Controller, BoxedUnit>() {
          @Override
          public BoxedUnit apply(Controller controller) {
            update(getList(controller.elements()));
            return null;
          }
        };
    this.sideWalkController.addObserver(f);

    for (int i = 0; i < SideWalkController.NUM_COLUMNS; i++) {
      for (int j = 0; j < SideWalkController.NUM_ROWS; j++) {
        this.worldGrid.add(new AnchorPane(), i, j);
      }
    }

    this.worldGrid
        .getChildren()
        .forEach(
            node -> {
              if (GridPane.getRowIndex(node) == 0) {
                node.setStyle("-fx-background-color: lightblue;");
              } else {
                node.setStyle("-fx-background-color: green;");
              }
            });

    this.worldGrid.setAlignment(Pos.CENTER);
  }

  /**
   * Method that draws a person on the stage
   *
   * @param person the vehicle that has to be drawn
   */
  private void drawPerson(final Person person) {
    if (this.getNodeByID(this.mainPane, person.id()) == null) {
      ImageView image;
      if (person.direction().equals(DirectionEnum.EAST())) {
        image = new ImageView("/view/images/man-walking.png");
      } else {
        image = new ImageView("/view/images/man-walking-mirrored.png");
      }
      HBox hbox = new HBox(image);
      hbox.setLayoutX(person.position().column() * 100);
      hbox.setLayoutY(160);
      hbox.setId(person.id());

      this.mainPane.getChildren().add(hbox);
    } else {
      this.movePerson(person);
    }
  }

  /**
   * Method that draws street on the stage
   *
   * @param cellList list of element to be drawn
   */
  private void drawStreet(final List<Cell> cellList) {
    cellList.forEach(
        cell -> {
          Node currentNode =
              ScenarioControllerUtilities.getNodeByRowColumnIndex(
                  cell.column(), cell.row(), this.worldGrid);
          currentNode.setStyle("-fx-background-color: grey;");
        });
  }

  /**
   * Method for drawing street lamp
   *
   * @param id the street lamp id
   * @param state the street lamp state
   */
  private void drawStreetLamp(final String id, final String state, final Cell position) {
    Node semaphoreNode =
        ScenarioControllerUtilities.getNodeByRowColumnIndex(
            position.column(), position.row(), this.worldGrid);
    ImageView semaphoreImage = getStreetLampImage(state);
    HBox hBox = new HBox(semaphoreImage);

    semaphoreNode.setStyle("-fx-background-color: lightblue;");
    hBox.setId(id);
    ((AnchorPane) semaphoreNode).getChildren().add(hBox);
  }

  /**
   * Getter for street lamp image
   *
   * @param state the street lamp state
   * @return the street lamp image
   */
  private ImageView getStreetLampImage(final String state) {
    ImageView streetLampImage = new ImageView();
    if (state.equals(StreetLampStates.LOW())) {
      streetLampImage = new ImageView("/view/images/street-lamp-low.png");
    } else if (state.equals(StreetLampStates.MEDIUM())) {
      streetLampImage = new ImageView("/view/images/street-lamp-medium.png");
    } else if (state.equals(StreetLampStates.HIGH())) {
      streetLampImage = new ImageView("/view/images/street-lamp-high.png");
    }
    return streetLampImage;
  }

  /**
   * Method for painting the scenario
   *
   * @param elements the list of basic element to be drawn
   */
  private void paintScenario(List<BasicElement> elements) {
    elements.forEach(
        elem -> {
          if (elem instanceof Person) {
            Person person = (Person) elem;
            this.drawPerson(person);
          } else if (elem instanceof StreetElement) {
            StreetElement streetElement = (StreetElement) elem;
            this.drawStreet(this.getList(streetElement.cellList()));
          } else if (elem instanceof StreetLamp) {
            StreetLamp streetLamp = (StreetLamp) elem;
            this.drawStreetLamp(streetLamp.id(), streetLamp.state(), streetLamp.position());
          }
        });
  }

  @FXML
  void initialize() {
    assert mainPane != null
        : "fx:id=\"mainPane\" was not injected: check your FXML file 'SideWalk.fxml'.";
    assert worldGrid != null
        : "fx:id=\"worldGrid\" was not injected: check your FXML file 'SideWalk.fxml'.";

    this.initScenario();
    this.paintScenario(this.getList(this.sideWalkController.elements()));
  }
}
