package view;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

import java.util.List;

/**
 * Interface for scenario's controllers
 */
public interface ScenarioController {

    /**
     * Getter for start button
     *
     * @return the start button
     */
    Button getStartButton();

    /**
     * Getter for pause button
     *
     * @return the pause button
     */
    Button getPauseButton();

    /**
     * Set the running application
     *
     * @param isRunning true if the application is running, false otherwise
     */
    void setRunning(boolean isRunning);

    /**
     * Method for starting controller
     */
    void startController();

    /**
     * Method for starting simulation
     */
    void startSimulation();

    /**
     * Method for pause simulation
     */
    void pauseSimulation();

    /**
     * Getter for list of element converted in java list
     *
     * @param list list in scala format
     * @return the list in java format
     */
    List getList(scala.collection.immutable.List list);

    /**
     * Getter for node
     *
     * @param pane the pane of the stage
     * @param id   the id of the node
     * @return the node interested
     */
    Node getNodeByID(final Pane pane, final String id);
}
