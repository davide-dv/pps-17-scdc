package projectutilities

import model.components.actuators.{Semaphore, SemaphoreInformation, StreetLamp}
import model.components.basic.{Cell, Person, Vehicle}
import model.components.messages._
import model.components.sensors.Sensor
import play.api.libs.json._

/**
  * Defines Json utilities for Json library
  */
trait JsonUtilities {

  implicit val semaphoreInformationReads: Reads[SemaphoreInformation] =
    Json.reads[SemaphoreInformation]
  implicit val semaphoreInformationWrites: OWrites[SemaphoreInformation] =
    Json.writes[SemaphoreInformation]

  implicit val cellMessageReads: Reads[Cell] = Json.reads[Cell]
  implicit val cellMessageWrites: OWrites[Cell] = Json.writes[Cell]

  implicit val vehicleMessageReads: Reads[Vehicle] = Json.reads[Vehicle]
  implicit val vehicleMessageWrites: OWrites[Vehicle] = Json.writes[Vehicle]

  implicit val semaphoreMessageReads: Reads[Semaphore] = Json.reads[Semaphore]
  implicit val semaphoreMessageWrites: OWrites[Semaphore] =
    Json.writes[Semaphore]

  implicit val PersonMessageReads: Reads[Person] = Json.reads[Person]
  implicit val PersonMessageWrites: OWrites[Person] =
    Json.writes[Person]

  implicit val StreetLampMessageReads: Reads[StreetLamp] = Json.reads[StreetLamp]
  implicit val StreetLampMessageWrites: OWrites[StreetLamp] =
    Json.writes[StreetLamp]

  implicit val sensorMessageReads: Reads[Sensor] = Json.reads[Sensor]
  implicit val sensorMessageWrites: OWrites[Sensor] = Json.writes[Sensor]

  implicit val crossRoadEnvironmentMessageReads: Reads[CrossRoadEnvironment] =
    Json.reads[CrossRoadEnvironment]
  implicit val crossRoadEnvironmentMessageWrites: OWrites[CrossRoadEnvironment] =
    Json.writes[CrossRoadEnvironment]

  implicit val sideWalkEnvironmenMessageReads: Reads[SideWalkEnvironment] =
    Json.reads[SideWalkEnvironment]
  implicit val sideWalkEnvironmenMessageWrites: OWrites[SideWalkEnvironment] =
    Json.writes[SideWalkEnvironment]

  implicit val environmentsMessageReads: Reads[Environments] =
    Json.reads[Environments]
  implicit val environmentsMessageWrites: OWrites[Environments] =
    Json.writes[Environments]

  implicit val environmentMessageWrites: OWrites[EnvironmentMessage] =
    Json.writes[EnvironmentMessage]
  implicit val environmentMessageReads: Reads[EnvironmentMessage] =
    Json.reads[EnvironmentMessage]

  implicit val elementInfoMessageWrites: OWrites[ElementInfo] =
    Json.writes[ElementInfo]
  implicit val elementInfoMessageReads: Reads[ElementInfo] =
    Json.reads[ElementInfo]

  implicit val genericMessageReads: Reads[GenericMessage] =
    Json.reads[GenericMessage]
  implicit val genericMessageWrites: OWrites[GenericMessage] =
    Json.writes[GenericMessage]

  implicit val commandMessageReads: Reads[CommandMessage] =
    Json.reads[CommandMessage]
  implicit val commandMessageWrites: OWrites[CommandMessage] =
    Json.writes[CommandMessage]


}
