package projectutilities

import controller.basic.Scenario
import controller.basic.Scenario.Scenario
import model.components.basic.Element
import model.components.messages.EnvironmentMessage

/**
  * Object for message utils
  */
object MessageUtilities {

  /**
    * Method to update the environment
    *
    * @param elements           the list of elements
    * @param environmentMessage the environment message
    * @param scenario           the scenario used
    * @return the list of element updated
    */
  def updateEnvironment(elements: List[Element], environmentMessage: EnvironmentMessage,
                        scenario: Scenario): List[Element] = {
    var envElements: List[Element] = List()
    if (scenario == Scenario.SIDE_WALK) {
      val environment =
        environmentMessage.environments.sideWalkEnvironment.get
      envElements = environment.streetLamps ::: environment.people ::: environment.sensors
    } else {
      val environment =
        environmentMessage.environments.crossRoadEnvironment.get
      envElements = environment.semaphores ::: environment.vehicles ::: environment.sensors
    }
    var result: List[Element] = elements
    var contains: Boolean = false
    if (envElements.size > elements.size) {
      for (elem <- envElements) {
        result.foreach(e => {
          if (e.id == elem.id) {
            contains = true
          }
        })
        if (!contains) {
          result = elem :: result
        }
        contains = false
      }
    } else {
      for (elem <- elements) {
        envElements.foreach(e => {
          if (e.id == elem.id) {
            contains = true
          }
        })
        if (!contains) {
          result = result.filter(el => el.id != elem.id)
        }
        contains = false
      }
    }
    result

  }
}
