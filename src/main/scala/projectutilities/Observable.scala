package projectutilities

/**
  * Trait for observable element
  *
  * @tparam S generic type of observable
  */
trait Observable[S] {
  this: S =>
  private var observers: List[S => Unit] = Nil

  /**
    * Add an observer to the list of observers
    *
    * @param observer the observer to be added
    */
  def addObserver(observer: S => Unit): Unit = observers = observer :: observers

  /**
    * Method for notifies all the observers added
    */
  def notifyObservers(): Unit = observers.foreach(_.apply(this))
}