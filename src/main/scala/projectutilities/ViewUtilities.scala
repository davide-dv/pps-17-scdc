package projectutilities

object ViewUtilities {

  val SETTINGS_WIDTH = 490
  val SETTINGS_HEIGHT = 350
  val MENU_WIDTH = 550
  val MENU_HEIGHT = 150
  val CROSSROAD_SCENARIO_TITLE = "CrossRoad Scenario"
  val SIDEWALK_SCENARIO_TITLE = "SideWalk Scenario"
  val CROSSROAD_SETTINGS_TITLE = "CrossRoad Settings"
  val LOADING_TEXT = "Loading "
  val LOADING_TIME = 7000
  val SETTINGS_CSS_PATH = "view/css/settings.css"
  val MENU_CSS_PATH = "view/css/menu.css"
  val MENU_TITLE = "Smart City Devices Coordinator"
  val CROSSROAD_CSS_PATH = "view/css/crossRoad.css"
  val SIDEWALK_CSS_PATH = "view/css/sideWalk.css"
  val SELECT_SCENARIO_TEXT = "Select Scenario: "
  val TITLE_IMAGE_PATH = "/view/images/scdc.png"
  val CAR_ICON_PATH = "/view/images/car.png"
  val MAN_WALKING_ICON_PATH = "/view/images/man-walking.png"
  val SETTINGS_ICON_PATH = "/view/images/settings-icon.png"
  val BUTTON_SHOW_TEXT = "Show"
  val DELETE_CONFIRMATION_TEXT =
    "Delete configuration and all RabbitMQ queues?"
  val SUCCESS_ELIMINATION_TEXT = "Done!"
  val CROSSROAD_FXML_PATH = "/view/fxml/CrossRoad.fxml"
  val SETTINGS_FXML_PATH = "/view/fxml/CrossRoadSettings.fxml"
  val SIDEWAL_FXML_PATH = "/view/fxml/SideWalk.fxml"
  val LOADING_TEXT_ID = "loadingText"
  val SETTINGS_BUTTON_ID = "settingsButton"
  val DELETE_ALL_QUEUES_BUTTON_ID = "deleteAllQueuesButton"
  val SELECT_SCENARIO_TEXT_ID = "selectScenario"
  val loadingArrayCR =
    Array("Streets", "Sensors", "Vehicles", "Semaphores", "Scenario")
  val loadingArraySW =
    Array("Streets", "Lamps", "People", "Scenario")

}
