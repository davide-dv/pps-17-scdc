package model.components.actuators

import java.util.UUID.randomUUID

import model.components.basic.LayerEnum.Layer
import model.components.basic.{Cell, DirectionEnum, Element, LayerEnum}

/**
  * Enum for colors
  */
object ColorEnum extends Enumeration {
  type Color = String
  val RED = "Red"
  val YELLOW = "Yellow"
  val GREEN = "Green"
}

/**
  * Class that defines semaphore information
  *
  * @param state                 is the current state of the semaphore
  * @param lastWaitingDetectedCar passed time from the last car detected by the semaphore
  * @param lastChangeStateTime    the last time that semaphore has changed
  */
case class SemaphoreInformation(var state: ColorEnum.Color, var lastWaitingDetectedCar: Long = 0,
                                var lastChangeStateTime: Long = System.currentTimeMillis())

/**
  * Defines a semaphore
  */
trait Semaphore extends Ordered[Integer] with Element {

  /**
    * Getter for semaphore information
    */
  var semaphoreInformation: SemaphoreInformation

  /**
    * Getter that is not Option.empty if is the semaphore is
    * the one who has been waiting for more
    */
  var longAwaitedSemaphore: Option[Semaphore]

  /**
    * Set semaphore information
    *
    * @param newSemaphoreInformation the new semaphore information to be set
    */
  def semaphoreInformation_(newSemaphoreInformation: SemaphoreInformation): Unit =
    semaphoreInformation = newSemaphoreInformation

  /**
    * Current layer of a semaphore
    */
  override var layer: Layer = LayerEnum.SEMAPHORE_LAYER
}

/**
  * Object for a semaphore
  */
object Semaphore {
  /**
    * Creates a semaphore with given parameters
    *
    * @param position             the position on the board
    * @param direction            the direction on the board
    * @param semaphoreInformation the semaphore information
    * @param id                   the semaphore id
    * @return a new SemaphoreActuator instance
    */
  def apply(position: Cell,
            direction: DirectionEnum.Direction,
            semaphoreInformation: SemaphoreInformation,
            id: String = randomUUID().toString): Semaphore =
    new SemaphoreActuator(position, direction, semaphoreInformation, id)

  /**
    * Destroys a SemaphoreActuator instance
    *
    * @param arg the semaphore
    * @return an Optional for destroying semaphore
    */
  def unapply(arg: Semaphore): Option[(Cell, DirectionEnum.Direction, SemaphoreInformation, String)] =
    Some((arg.position, arg.direction, arg.semaphoreInformation, arg.id))

  /**
    * Instance to be created
    *
    * @param position             the position on the board
    * @param direction            the direction on the board
    * @param semaphoreInformation the semaphore information
    * @param id                   the semaphore id
    */
  private class SemaphoreActuator(var position: Cell, var direction: DirectionEnum.Direction,
                                  var semaphoreInformation: SemaphoreInformation, var id: String) extends Semaphore {

    override var longAwaitedSemaphore: Option[Semaphore] = Option.empty

    override def compare(that: Integer): Int = that match {
      case _ if this.semaphoreInformation.lastWaitingDetectedCar > that => 1
      case _ if this.semaphoreInformation.lastWaitingDetectedCar == that => 0
      case _ => -1
    }

    override def toString =
      s"SemaphoreActuator($id, $position, $direction, $semaphoreInformation)"
  }

}

