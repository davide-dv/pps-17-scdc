package model.components.actuators

import java.util.UUID.randomUUID

import model.components.basic.LayerEnum.Layer
import model.components.basic.{Cell, DirectionEnum, Element, LayerEnum}

/**
  * Enum for street lamp states
  */
object StreetLampStates extends Enumeration {
  type StreetLampState = String
  val LOW = "LOW"
  val MEDIUM = "MEDIUM"
  val HIGH = "HIGH"
}

/**
  * Defines a street lamp
  */
trait StreetLamp extends Element {

  /**
    * Current state of a street lamp
    */
  var state: StreetLampStates.StreetLampState

  /**
    * Set current street lamp state
    *
    * @param s the new street lamp state
    */
  def state_(s: StreetLampStates.StreetLampState): Unit = state = s

  /**
    * Current layer of street lamp
    */
  override var layer: Layer = LayerEnum.STREET_LAMP_LAYER
}

/**
  * Object for street lamp
  */
object StreetLamp {

  /**
    * Creates a new instance of StreetLampActuator
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param state    the street lamp information
    * @param id        the street lamp id
    * @return a new SemaphoreActuator instance
    */
  def apply(position: Cell,
            direction: DirectionEnum.Direction,
            state: StreetLampStates.StreetLampState = StreetLampStates.LOW,
            id: String = randomUUID().toString): StreetLamp =
    new StreetLampActuator(position, direction, state, id)

  /**
    * Destroys a StreetLampActuator instance
    *
    * @param arg the street lamp
    * @return an Optional for destroying street lamp
    */
  def unapply(arg: StreetLamp): Option[
    (Cell, DirectionEnum.Direction, StreetLampStates.StreetLampState, String)
  ] =
    Some((arg.position, arg.direction, arg.state, arg.id))

  /**
    * Instance to be created
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param state    the street lamp information
    * @param id        the street lamp id
    */
  private class StreetLampActuator(var position: Cell,
                                   var direction: DirectionEnum.Direction,
                                   var state: StreetLampStates.StreetLampState,
                                   var id: String)
      extends StreetLamp {
    override def toString =
      s"StreetLampActuator($id, $position, $direction, $state)"
  }

}
