package model.components.sensors

import java.util.UUID.randomUUID

import model.components.basic.DirectionEnum.Direction
import model.components.basic.LayerEnum.Layer
import model.components.basic.{Cell, Element, LayerEnum}
import model.components.sensors.SensorEnum.{FAULT, OFF, ON, State}

/**
  * Enum for sensor's state
  */
object SensorEnum extends Enumeration {
  type State = String
  val ON = "ON"
  val OFF = "OFF"
  val FAULT = "FAULT"
}

/**
  * Defines a sensor
  */
trait Sensor extends Element {

  /**
    * Current state of a sensor
    */
  var state: State = OFF

  /**
    * Current layer of a sensor
    */
  override var layer: Layer = LayerEnum.SENSOR_LAYER

  /**
    * Current position of a sensor
    */
  override var position: Cell

  /**
    * Current direction of a sensor
    */
  override var direction: Direction

  /**
    * Set sensor state
    *
    * @param s the new state that the sensor will assume
    */
  def state_(s: State): Unit = state = s

  /**
    * Verify that the sensor is on
    *
    * @return true if sensor state is ON, false otherwise
    */
  def isOn: Boolean = if (state == ON) true else false

  /**
    * Verify that the sensor is fault
    *
    * @return true if sensor state is FAULT, false otherwise
    */
  def isDamaged: Boolean = if (state == FAULT) true else false
}

/**
  * The object that identify a sensor
  */
object Sensor {

  /**
    * Creates a new instance of Sensor
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param id        the sensor id
    * @return a new Sensor instance
    */
  def apply(position: Cell,
            direction: Direction,
            id: String = randomUUID().toString): Sensor =
    SensorImpl(position, direction, id)

  /**
    * Destroys an instance
    *
    * @param arg the sensor
    * @return an Optional for destroying sensor
    */
  def unapply(arg: Sensor): Option[(Cell, Direction, String)] =
    Some(arg.position, arg.direction, arg.id)

  /**
    * Instance to be created
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param id        the sensor id
    */
  private case class SensorImpl(override var position: Cell,
                                override var direction: Direction,
                                var id: String)
      extends Sensor {

    override def toString =
      s"SensorImpl($state, $layer, $position, $direction, $id)"
  }

}
