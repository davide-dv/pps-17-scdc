package model.components.basic

import java.util.UUID.randomUUID

import model.components.basic.LayerEnum.Layer

/**
  * Enum for layers
  */
object LayerEnum extends Enumeration {
  type Layer = String
  val STREET_LAYER = "STREET_LAYER"
  val SENSOR_LAYER = "SENSOR_LAYER"
  val VEHICLE_LAYER = "VEHICLE_LAYER"
  val PERSON_LAYER = "PERSON_LAYER"
  val SEMAPHORE_LAYER = "SEMAPHORE_LAYER"
  val STREET_LAMP_LAYER = "STREET_LAMP_LAYER"
}

/**
  * Defines an element
  */
trait Element extends BasicElement {

  /**
    * Current element position
    */
  var position: Cell

  /**
    * Set element position
    *
    * @param c the new cell that the element will assume
    */
  def position_(c: Cell): Unit = position = c

}

/**
  * The object that identifies an element
  */
object Element {

  /**
    * Creates a new instance of Element
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param layer     the layer
    * @param id        the element id
    * @return a new Element instance
    */
  def apply(position: Cell,
            direction: DirectionEnum.Direction,
            layer: LayerEnum.Layer,
            id: String = randomUUID().toString): Element =
    ElementImpl(position, direction, layer, id)

  /**
    * Destroys an instance
    *
    * @param arg the element
    * @return an Optional for element destroyed
    */
  def unapply(
    arg: Element
  ): Option[(Cell, DirectionEnum.Direction, LayerEnum.Layer, String)] =
    Some((arg.position, arg.direction, arg.layer, arg.id))

  /**
    * Instance to be created
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param layer     the layer
    * @param id        the element id
    */
  private case class ElementImpl(var position: Cell,
                                 var direction: DirectionEnum.Direction,
                                 var layer: LayerEnum.Layer,
                                 var id: String)
      extends Element {
    require(position != null && direction != null, "Not null fields required")
  }

}

/**
  * Defines a person
  */
trait Person extends Element

/**
  * Object for a person
  */
object Person {

  /**
    * Creates a new instance of Person
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param id        the person id
    * @return a new Person instance
    */
  def apply(position: Cell,
            direction: DirectionEnum.Direction,
            id: String = randomUUID().toString): Person =
    new PersonImpl(position, direction, id)

  /**
    * Destroys an instance
    *
    * @param arg the person
    * @return an Optional for destroying person
    */
  def unapply(arg: Person): Option[(Cell, DirectionEnum.Direction, String)] =
    Some((arg.position, arg.direction, arg.id))

  /**
    * Instance to be created
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param id        the person id
    */
  class PersonImpl(var position: Cell,
                   var direction: DirectionEnum.Direction,
                   var id: String)
      extends Person {

    override var layer: Layer = LayerEnum.PERSON_LAYER

    override def toString = s"PersonImpl($layer, $position, $direction, $id)"
  }

}

/**
  * Defines a vehicle
  */
trait Vehicle extends Element

/**
  * Object for a vehicle
  */
object Vehicle {

  /**
    * Creates a new instance of Vehicle
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param id        the vehicle id
    * @return a new Vehicle instance
    */
  def apply(position: Cell,
            direction: DirectionEnum.Direction,
            id: String = randomUUID().toString): Vehicle =
    new VehicleImpl(position, direction, id)

  /**
    * Destroys an instance
    *
    * @param arg the vehicle
    * @return an Optional for destroying vehicle
    */
  def unapply(arg: Element): Option[(Cell, DirectionEnum.Direction, String)] =
    Some((arg.position, arg.direction, arg.id))

  /**
    * Instance to be created
    *
    * @param position  the position on the board
    * @param direction the direction on the board
    * @param id        the vehicle id
    */
  class VehicleImpl(var position: Cell,
                    var direction: DirectionEnum.Direction,
                    var id: String)
      extends Vehicle {
    override var layer: Layer = LayerEnum.VEHICLE_LAYER

    override def toString = s"VehicleImpl($layer, $id, $position, $direction)"
  }

}
