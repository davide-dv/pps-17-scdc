package model.components.basic

/**
  * Enum for direction
  */
object DirectionEnum extends Enumeration {
  type Direction = String
  val NORTH = "NORTH"
  val SOUTH = "SOUTH"
  val EAST = "EAST"
  val WEST = "WEST"
}

/**
  * Defines a basic element
  */
trait BasicElement {

  /**
    * Current direction of a basic element
    */
  var direction: DirectionEnum.Direction

  /**
    * Set a new direction for basic element
    *
    * @param d the new direction that basic element will assume
    */
  def direction_(d: DirectionEnum.Direction): Unit = direction = d

  /**
    * Current layer of a basic element
    */
  var layer: LayerEnum.Layer

  /**
    * Set a new layer for basic element
    *
    * @param l the new layer that basic element will assume
    */
  def layer_(l: LayerEnum.Layer): Unit = layer = l

  /**
    * Id for basic element
    */
  var id: String
}
