package model.components.basic

import java.util.UUID.randomUUID

import model.components.basic.LayerEnum.Layer

/**
  * Defines a street
  */
trait StreetElement extends BasicElement {

  /**
    * Getter for the list of cells covered by the street
    */
  var cellList: List[Cell]

  /**
    * Set street's cell
    *
    * @param l the new cellList
    */
  def cellList_(l: List[Cell]): Unit = cellList = l

  /**
    * Add a cell to cellList
    *
    * @param c the cell to be added to cellList
    */
  def addCell(c: Cell): Unit = cellList_(c :: cellList)

  /**
    * Current street layer
    */
  override var layer: Layer = LayerEnum.STREET_LAYER
}

/**
  * Object for a street
  */
object Street {
  /**
    * Creates a new instance of Street
    *
    * @param cells     the cells for the street
    * @param direction the direction on the board
    * @param id        the street id
    * @return a new Street instance
    */
  def apply(cells: List[Cell], direction: DirectionEnum.Direction,
            id: String = randomUUID().toString): StreetElement = StreetElementImpl(cells, direction, id)

  /**
    * Destroys an instance
    *
    * @param arg the street
    * @return an Optional for street destroyed
    */
  def unapply(arg: StreetElement): Option[(List[Cell], DirectionEnum.Direction, LayerEnum.Layer, String)] =
    Some((arg.cellList, arg.direction, arg.layer, arg.id))

  /**
    * Instance to be created
    *
    * @param cellList  the cells for the street
    * @param direction the direction on the board
    * @param id        the street id
    */
  private case class StreetElementImpl(var cellList: List[Cell], var direction: DirectionEnum.Direction,
                                       var id: String) extends StreetElement {

    override def toString = s"Street($layer, $direction, $id)"
  }

}
