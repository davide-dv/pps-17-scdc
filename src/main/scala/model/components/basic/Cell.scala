package model.components.basic

/**
  * Defines a Cell
  */
trait Cell {

  /**
    * Getter for column value
    *
    * @return integer for column value
    */
  def column: Int

  /**
    * Getter for row value
    *
    * @return integer for row value
    */
  def row: Int

  /**
    * Getter for cell's position
    *
    * @return a pair composed by column and row
    */
  def position: (Int, Int)
}

/**
  * The object that identify a cell
  */
object Cell {

  /**
    * Creates a new instance of CellImpl
    *
    * @param column cell column
    * @param row    cell row
    * @return a new instance of Cell
    */
  def apply(column: Int, row: Int): Cell = CellImpl(column, row)

  /**
    * Destroys an instance
    *
    * @param c the cell
    * @return an Optional for destroying cell
    */
  def unapply(c: Cell): Option[(Int, Int)] = Some((c.column, c.row))

  /**
    * Instance to be created
    *
    * @param column cell column
    * @param row    cell row
    */
  private case class CellImpl(override val column: Int, override val row: Int)
      extends Cell {
    override def position: (Int, Int) = (column, row)

    override def toString: String =
      "Column:" + column + " Row:" + row + " -> Position: Cell" + position

  }

}
