package model.components.messages

import model.components.actuators.{Semaphore, StreetLamp}
import model.components.basic.DirectionEnum.Direction
import model.components.basic.{Cell, Element, Person, Vehicle}
import model.components.messages.Commands.Command
import model.components.sensors.Sensor

/**
  * Enum for commands
  */
object Commands extends Enumeration {
  type Command = String
  val START = "start"
  val STOP = "stop"
  val UPDATE = "update"
}

/**
  * Enum for message typology
  */
object MessageTypology extends Enumeration {
  type Type = String
  val STATE_MSG = "STATE MESSAGE"
  val SYNCH_MSG = "SYNCH MESSAGE"
}

/**
  * Defines a message
  */
trait Message

/**
  * Class for command messages, used to start, pause or update the scenario
  *
  * @param command            is the command to be sent
  * @param environmentMessage if present is the environment, Option.empty otherwise
  */
case class CommandMessage(command: Command,
                          environmentMessage: Option[EnvironmentMessage] =
                            Option.empty)
    extends Message

/**
  * Class for element info messages, used to send information about an element
  *
  * @param id        the element id
  * @param direction the element direction
  */
case class ElementInfo(id: String, direction: Direction) extends Message

/**
  * Class for elements messages, used to send the current list of elements
  *
  * @param elements the list to be sent
  */
case class ElementsMessage(elements: List[Element]) extends Message

/**
  * Class for environment messages, used to update the environment
  *
  * @param environments the environment referring to
  * @param msgOperation the operation, default is empty
  * @param index        the index to navigate environment, default 0
  */
case class EnvironmentMessage(environments: Environments,
                              msgOperation: String = "",
                              index: Int = 0)
    extends Message

/**
  * Class for define the environment used to define scenario
  *
  * @param crossRoadEnvironment is not Option.empty if the scenario is cross road
  * @param sideWalkEnvironment  is not Option.empty if the scenario is side walk
  */
case class Environments(
  crossRoadEnvironment: Option[CrossRoadEnvironment] = Option.empty,
  sideWalkEnvironment: Option[SideWalkEnvironment] = Option.empty
) extends Message

/**
  * Class for cross road environment messages, used to synchronize objects in cross road scenario
  *
  * @param semaphores updated list of semaphores
  * @param vehicles   updated list of vehicles
  * @param sensors    updated list of sensors
  */
case class CrossRoadEnvironment(semaphores: List[Semaphore],
                                vehicles: List[Vehicle],
                                sensors: List[Sensor])
    extends Message

/**
  * Class for side walk environment messages, used to synchronize objects in side walk scenario
  *
  * @param people      updated list of people
  * @param streetLamps updated list of streetLamps
  * @param sensors     updated list of sensors
  */
case class SideWalkEnvironment(people: List[Person],
                               streetLamps: List[StreetLamp],
                               sensors: List[Sensor])
    extends Message

/**
  * Class for generic messages, used to exchange generic information
  *
  * @param elementInfo     element information
  * @param elementPosition element position
  * @param elementState    element state
  * @param messageTypology message typology, default STATE_MSG
  */
case class GenericMessage(elementInfo: ElementInfo,
                          elementPosition: Cell,
                          elementState: String = "",
                          messageTypology: MessageTypology.Type =
                            MessageTypology.STATE_MSG)
    extends Message

/**
  * Class for register messages, used to register an element to register topic
  *
  * @param element          the element to be registered
  * @param elementOperation the operation
  */
case class RegisterMessage(element: Element, elementOperation: String)
