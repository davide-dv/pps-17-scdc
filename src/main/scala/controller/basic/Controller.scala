package controller.basic

import controller.actors.{FSMActor, RegisterActor, SimulationActor}
import controller.basic.Scenario.Scenario
import model.components.basic._
import projectutilities.Observable
import view.controllersutilities.CrossRoadSettings


/**
  * Enum for scenarios
  */
object Scenario extends Enumeration {
  type Scenario = String
  val CROSS_ROAD = "Cross Road"
  val SIDE_WALK = "Side Walk"
}


/**
  * Defines a Controller
  */
trait Controller extends Observable[Controller] {

  /**
    * Selected scenario
    *
    * @return the scenario
    */
  def scenario: Scenario

  /**
    * Board's size
    *
    * @return an array with width and height
    */
  def gridSize: (Int, Int)

  /**
    * List of basic element on the board
    */
  var elements: List[BasicElement]

  /**
    * list of element's actors
    */
  val elementActors: List[FSMActor]

  /**
    * SimulationActor variable
    */
  val runner: SimulationActor

  /**
    * Runs th simulation
    */
  def startSimulation(): Unit

  /**
    * Stops the simulation
    */
  def stopSimulation(): Unit
}

/**
  * Companion object for Controller
  */
object Controller {

  /**
    * Creates a new ControllerImpl instance
    *
    * @param gridSize an array for board's size
    * @param scenario the chosen scenario
    * @return a new instance of ControllerImpl
    */
  def apply(gridSize: (Int, Int),
            scenario: Scenario,
            settings: Option[CrossRoadSettings]): Controller =
    new ControllerImpl(
      gridSize,
      scenario,
      ElementsInitStrategy(gridSize, scenario, settings).init()
    )

  /**
    * Destroys a ControllerImpl instance
    *
    * @param controller instance to be destroyed
    * @return an Option with parameters of ControllerImpl
    */
  def unapply(
    controller: Controller
  ): Option[((Int, Int), Scenario, List[BasicElement])] =
    Some(controller.gridSize, controller.scenario, controller.elements)

  /**
    * Instance for ControllerImpl
    *
    * @param gridSize an array for board's size
    * @param scenario the chosen scenario
    * @param elements list of elements
    */
  private class ControllerImpl(override val gridSize: (Int, Int),
                               override val scenario: Scenario,
                               override var
                               elements: List[BasicElement])
      extends Controller {

    val register = RegisterActor(scenario)
    this.register.init()

    Thread.sleep(500)

    override val runner: SimulationActor = SimulationActor(
      elements
        .filter(e => e.layer != LayerEnum.STREET_LAYER)
        .map(e => e.asInstanceOf[Element]),
      scenario
    )
    this.runner.init()

    override val elementActors: List[FSMActor] = {
      ElementActorsInitStrategy(
        elements
          .filter(e => e.layer != LayerEnum.STREET_LAYER)
          .map(e => e.asInstanceOf[Element]),
        scenario
      ).init()
    }

    override def startSimulation(): Unit = {
      this.runner.start()
    }

    override def stopSimulation(): Unit = {
      this.runner.stop()
    }

    this.runner.addObserver(_ => {
      elements = this.runner.updatedElements ::: elements.filter(
        e => e.layer == LayerEnum.STREET_LAYER
      )
      notifyObservers()
    })
  }

}
