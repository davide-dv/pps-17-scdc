package controller.basic

import controller.actors.FSMActor
import controller.actors.utilities.TopicEnum
import model.components.basic.{Cell, _}

/**
  * Creates new run time elements
  */
class NewElementInit {

  var cellList: List[(Cell, DirectionEnum.Direction)] =
    List(
      (Cell(3, -1), DirectionEnum.SOUTH),
      (Cell(-1, 4), DirectionEnum.EAST),
      (Cell(8, 3), DirectionEnum.WEST),
      (Cell(4, 7), DirectionEnum.NORTH)
    )

  /**
    * Verifies if in the new element position is empty or not
    *
    * @param elements   the list of basic elements on the board
    * @param newElemPos the new element position
    * @return true if is empty, false otherwise
    */
  def checkNewVehiclePosition(elements: java.util.List[BasicElement],
                              newElemPos: Cell): Boolean = {
    elements.forEach {
      case el: Vehicle => if (el.position == newElemPos) return true
      case _ =>
    }
    false
  }

  /**
    * Initialize a new vehicle;
    * 1) It creates a new Vehicle instance
    * 2) It creates a new FSMActor instance with Vehicle created before
    * 3) The actor is initialized
    * 4) the actor subscribe itself on vehicle's topics
    *
    * @param elements list of basic elements on the board
    */
  def initNewVehicle(elements: java.util.List[BasicElement]): Unit = {
    val rnd = new scala.util.Random
    val newVehicleInfo = this.cellList(rnd.nextInt(4))
    if (!checkNewVehiclePosition(elements, newVehicleInfo._1)) {
      val newVehicle = Vehicle(newVehicleInfo._1, newVehicleInfo._2)
      val newVehicleActor = FSMActor(newVehicle, Scenario.CROSS_ROAD)
      newVehicleActor.init()
      newVehicleActor.subscribe(
        List(
          TopicEnum.SCDC_COMMAND_TOPIC,
          TopicEnum.SCDC_SENSOR_TOPIC,
          TopicEnum.SCDC_VEHICLE_SYNCH_TOPIC,
          TopicEnum.SCDC_SEMAPHORE_TOPIC
        )
      )
    }
  }

  /**
    * Initialize a new person;
    * 1) It creates a new Person instance
    * 2) It creates a new FSMActor instance with Person created before
    * 3) The actor is initialized
    * 4) the actor subscribe itself on person's topics
    *
    * @param bool true if direction is East, false is West
    */
  def initNewPerson(bool: Boolean): Unit = {
    var position: Cell = Cell(-1, -1)
    var direction: DirectionEnum.Direction = DirectionEnum.NORTH
    if (bool) {
      position = Cell(-1, 1)
      direction = DirectionEnum.EAST
    } else {
      position = Cell(8, 1)
      direction = DirectionEnum.WEST
    }
    val newPerson = Person(position, direction)
    val newPersonActor = FSMActor(newPerson, Scenario.SIDE_WALK)
    newPersonActor.init()
    newPersonActor.subscribe(
      List(
        TopicEnum.SCDC_COMMAND_TOPIC,
        TopicEnum.SCDC_SENSOR_TOPIC,
        TopicEnum.SCDC_STREET_LAMP_TOPIC
      )
    )
  }
}
