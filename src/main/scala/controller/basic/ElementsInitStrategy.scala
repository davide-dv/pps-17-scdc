package controller.basic

import controller.basic.Scenario.Scenario
import model.components.actuators.{
  ColorEnum,
  Semaphore,
  SemaphoreInformation,
  StreetLamp
}
import model.components.basic._
import model.components.sensors.Sensor
import view.controllersutilities.CrossRoadSettings

/**
  * Defines a strategy for initialize elements
  */
sealed trait ElementsInitStrategy {

  /**
    *   CrossRoad Scenario Settings
    *
    * @return the settings of the scenario
    */
  def settings: Option[CrossRoadSettings]

  /**
    * Board's size
    *
    * @return an array with width and height
    */
  def size: (Int, Int)

  /**
    * The chosen scenario
    *
    * @return the scenario
    */
  def scenario: Scenario

  /**
    * Initialize elements on controller
    *
    * @return the list of basic element
    */
  def init(): List[BasicElement]

}

/**
  * Companion object of element initialization
  */
object ElementsInitStrategy {

  /**
    * Creates a new instance of ElementInit
    *
    * @param size     an array of board's size
    * @param scenario the scenario chosen
    * @param settings the settings of the cross road scenario
    * @return a new instance of ElementInit
    */
  def apply(size: (Int, Int),
            scenario: Scenario,
            settings: Option[CrossRoadSettings]): ElementsInitStrategy =
    new ElementInit(size, scenario, settings)

  /**
    * Instance for initialization
    *
    * @param size     an array of board's size
    * @param scenario the scenario chosen
    * @param settings the settings of the cross road scenario
    */
  private class ElementInit(override val size: (Int, Int),
                            override val scenario: Scenario,
                            override val settings: Option[CrossRoadSettings])
      extends ElementsInitStrategy {

    require(size._1 > 0 && size._2 > 0, "Invalid values")

    override def init(): List[BasicElement] = {
      var result: List[BasicElement] = List()

      scenario match {
        case Scenario.CROSS_ROAD =>
          streetsInit(scenario)
          vehiclesInit(
            settings.get.getNorthVehicles,
            settings.get.getEastVehicles,
            settings.get.getSouthVehicles,
            settings.get.getWestVehicles
          )
          sensorsInit(scenario)
          semaphoresInit(
            settings.get.getNorthSemaphoreState,
            settings.get.getEastSemaphoreState,
            settings.get.getSouthSemaphoreState,
            settings.get.getWestSemaphoreState
          )
        case Scenario.SIDE_WALK =>
          streetsInit(scenario)
          sensorsInit(scenario)
          streetLampsInit()
          peopleInit()
        case _ =>
      }

      /**
        * StreetLamps initialization
        */
      def streetLampsInit(): Unit = {
        result =
          StreetLamp(Cell(0, 0), DirectionEnum.WEST) ::
            StreetLamp(Cell(2, 0), DirectionEnum.WEST) ::
            StreetLamp(Cell(4, 0), DirectionEnum.WEST) ::
            StreetLamp(Cell(6, 0), DirectionEnum.WEST) ::
            result
      }

      /**
        * People initialization
        */
      def peopleInit(): Unit = {
        result =
          Person(Cell(0, 1), DirectionEnum.EAST) ::
            Person(Cell(5, 1), DirectionEnum.WEST) ::
            Person(Cell(7, 1), DirectionEnum.WEST) ::
            result
      }

      /**
        * Streets initialization
        */
      def streetsInit(scenario: Scenario): Unit = {
        scenario match {
          case Scenario.CROSS_ROAD =>
            var street1: List[Cell] = List()
            var street2: List[Cell] = List()
            var street3: List[Cell] = List()
            var street4: List[Cell] = List()
            for (i <- 0 until size._2;
                 j <- size._1 / 2 - 1 until size._1 / 2 + 1) {
              if (j == 3) {
                street1 = Cell(i, j) :: street1
                street2 = Cell(j, i) :: street2
              } else {
                street3 = Cell(i, j) :: street3
                street4 = Cell(j, i) :: street4
              }
            }
            result =
              Street(street1, DirectionEnum.WEST) ::
                Street(street2, DirectionEnum.SOUTH) ::
                Street(street3, DirectionEnum.EAST) ::
                Street(street4, DirectionEnum.NORTH) ::
                result
          case Scenario.SIDE_WALK =>
            var street: List[Cell] = List()
            for (i <- 0 until size._1) {
              street = Cell(i, 1) :: street
            }
            result =
              Street(street, DirectionEnum.EAST) ::
                result
          case _ =>
        }

      }

      /**
        * Vehicles initialization
        */
      def vehiclesInit(getNorthVehicles: Int,
                       getEastVehicles: Int,
                       getSouthVehicles: Int,
                       getWestVehicles: Int): Unit = {
        for (i <- 0 until getNorthVehicles) {
          result = Vehicle(Cell(4, 5 + i), DirectionEnum.NORTH) :: result
        }
        for (i <- 0 until getEastVehicles) {
          result = Vehicle(Cell(2 - i, 4), DirectionEnum.EAST) :: result
        }
        for (i <- 0 until getSouthVehicles) {
          result = Vehicle(Cell(3, 2 - i), DirectionEnum.SOUTH) :: result
        }
        for (i <- 0 until getWestVehicles) {
          result = Vehicle(Cell(5 + i, 3), DirectionEnum.WEST) :: result
        }
      }

      /**
        * Sensors initialization
        */
      def sensorsInit(scenario: Scenario): Unit = {
        scenario match {
          case Scenario.CROSS_ROAD =>
            result =
              Sensor(Cell(0, 4), DirectionEnum.EAST) ::
                Sensor(Cell(1, 4), DirectionEnum.EAST) ::
                Sensor(Cell(2, 4), DirectionEnum.EAST) ::
                Sensor(Cell(5, 3), DirectionEnum.WEST) ::
                Sensor(Cell(6, 3), DirectionEnum.WEST) ::
                Sensor(Cell(7, 3), DirectionEnum.WEST) ::
                Sensor(Cell(3, 0), DirectionEnum.SOUTH) ::
                Sensor(Cell(3, 1), DirectionEnum.SOUTH) ::
                Sensor(Cell(3, 2), DirectionEnum.SOUTH) ::
                Sensor(Cell(4, 5), DirectionEnum.NORTH) ::
                Sensor(Cell(4, 6), DirectionEnum.NORTH) ::
                Sensor(Cell(4, 7), DirectionEnum.NORTH) ::
                result
          case Scenario.SIDE_WALK =>
            result =
              Sensor(Cell(0, 1), DirectionEnum.WEST) ::
                Sensor(Cell(1, 1), DirectionEnum.WEST) ::
                Sensor(Cell(2, 1), DirectionEnum.WEST) ::
                Sensor(Cell(3, 1), DirectionEnum.WEST) ::
                Sensor(Cell(4, 1), DirectionEnum.WEST) ::
                Sensor(Cell(5, 1), DirectionEnum.WEST) ::
                Sensor(Cell(6, 1), DirectionEnum.WEST) ::
                Sensor(Cell(7, 1), DirectionEnum.WEST) ::
                result
          case _ =>
        }

      }

      /**
        * Semaphores initialization
        */
      def semaphoresInit(getNorthSemaphoreState: ColorEnum.Color,
                         getEastSemaphoreState: ColorEnum.Color,
                         getSouthSemaphoreState: ColorEnum.Color,
                         getWestSemaphoreState: ColorEnum.Color): Unit = {
        result =
          Semaphore(
            Cell(4, 5),
            DirectionEnum.NORTH,
            SemaphoreInformation(getNorthSemaphoreState)
          ) ::
            Semaphore(
            Cell(2, 4),
            DirectionEnum.EAST,
            SemaphoreInformation(getEastSemaphoreState)
          ) ::
            Semaphore(
            Cell(3, 2),
            DirectionEnum.SOUTH,
            SemaphoreInformation(getSouthSemaphoreState)
          ) ::
            Semaphore(
            Cell(5, 3),
            DirectionEnum.WEST,
            SemaphoreInformation(getWestSemaphoreState)
          ) ::
            result
      }

      result
    }
  }

}
