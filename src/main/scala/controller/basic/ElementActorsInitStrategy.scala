package controller.basic

import controller.actors.FSMActor
import controller.actors.utilities.TopicEnum
import controller.basic.Scenario.Scenario
import model.components.actuators.{Semaphore, StreetLamp}
import model.components.basic.{Element, Person, Vehicle}
import model.components.sensors.Sensor

/**
  * Defines initialization for element's actors
  */
trait ElementActorsInitStrategy {

  def scenario: Scenario

  def elements: List[Element]

  def init(): List[FSMActor]

}

/**
  * Companion object for an instance of ElementActorsInitStrategyImpl
  */
object ElementActorsInitStrategy {

  /**
    * Creates a new instance of ElementActorsInitStrategyImpl
    *
    * @param elements the list of board's elements
    * @param scenario the chosen scenario
    * @return a new instance of ElementActorsInitStrategyImpl
    */
  def apply(elements: List[Element],
            scenario: Scenario): ElementActorsInitStrategy = new ElementActorsInitStrategyImpl(elements, scenario)

  /**
    * Instance of ElementActorsInitStrategyImpl
    *
    * @param elements the list of board's elements
    * @param scenario the chosen scenario
    */
  private class ElementActorsInitStrategyImpl(override val elements: List[Element],
                                              override val scenario: Scenario) extends ElementActorsInitStrategy {

    /**
      * Subscribes an element's actor to related topics
      *
      * @param FSMActor the elements's actor
      */
    def subscribeActor(FSMActor: FSMActor): Unit = {
      FSMActor.element match {
        case _: Semaphore =>
          FSMActor.subscribe(
            List(
              TopicEnum.SCDC_SENSOR_TOPIC,
              TopicEnum.SCDC_SEMAPHORE_SYNCH_TOPIC
            )
          )
        case _: Vehicle =>
          FSMActor.subscribe(
            List(
              TopicEnum.SCDC_COMMAND_TOPIC,
              TopicEnum.SCDC_SENSOR_TOPIC,
              TopicEnum.SCDC_VEHICLE_SYNCH_TOPIC,
              TopicEnum.SCDC_SEMAPHORE_TOPIC
            )
          )
        case _: Sensor =>
          FSMActor.subscribe(
            List(
              if (scenario == Scenario.CROSS_ROAD) TopicEnum.SCDC_VEHICLE_TOPIC
              else TopicEnum.SCDC_PERSON_TOPIC,
              TopicEnum.SCDC_COMMAND_TOPIC
            )
          )
        case _: StreetLamp =>
          FSMActor.subscribe(List(TopicEnum.SCDC_SENSOR_TOPIC))
        case _: Person =>
          FSMActor.subscribe(
            List(
              TopicEnum.SCDC_COMMAND_TOPIC,
              TopicEnum.SCDC_SENSOR_TOPIC,
              TopicEnum.SCDC_STREET_LAMP_TOPIC
            )
          )
        case _ => new IllegalStateException
      }
    }

    override def init(): List[FSMActor] = {

      val actors: List[FSMActor] = elements.map { el =>
        FSMActor(el, scenario)
      }

      actors.foreach(a => {
        a.init()
        subscribeActor(a)
      })

      actors

    }
  }

}
