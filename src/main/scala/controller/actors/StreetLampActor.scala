package controller.actors

import projectutilities.JsonUtilities
import controller.actors.behaviours.ActorBehaviour
import controller.actors.utilities.TopicEnum
import controller.actors.utilities.TopicEnum.Topic
import model.components.actuators.StreetLamp
import model.components.basic.Person
import model.components.messages._
import model.components.sensors.Sensor
import play.api.libs.json.Json

/**
  * Companion object for StreetLampActor
  */
object StreetLampActor {

  /**
    * Implicit and Helper for StreetLamp
    *
    * @param streetLamp the street lamp interested
    */
  implicit class StreetLampActor(streetLamp: StreetLamp)
    extends ActorBehaviour
      with JsonUtilities {

    override def stateMsgAction(streetLampActor: FSMActor,
                                topic: Topic,
                                msg: GenericMessage): Unit = {

      topic match {
        case TopicEnum.SCDC_SENSOR_TOPIC =>
          if (streetLampActor.actorUtility.get
            .msgList(topic)
            .size >= streetLampActor.elements
            .count(e => e.isInstanceOf[Sensor])) {

            import controller.actors.behaviours.actuators.StreetLampBehaviour._
            streetLamp.nextState(streetLampActor.elements)
            streetLampActor.actorUtility.get.msgList(topic) = List()
            if (streetLampActor.elements.count(el => el.isInstanceOf[Person]) == 0) {
              val ev = EnvironmentMessage(
                Environments(
                  sideWalkEnvironment = Option(
                    SideWalkEnvironment(
                      List(),
                      List(streetLamp),
                      streetLampActor.elements
                        .filter(e => e.isInstanceOf[Sensor])
                        .asInstanceOf[List[Sensor]]
                    )
                  )
                )
              )

              streetLampActor.write(
                Json.toJson(CommandMessage(Commands.UPDATE, Option(ev))),
                TopicEnum.SCDC_COMMAND_TOPIC
              )
            } else {
              streetLampActor.write(
                Json
                  .toJson(
                    GenericMessage(
                      ElementInfo(
                        streetLamp.id,
                        streetLamp.direction
                      ),
                      streetLamp.position,
                      streetLamp.state
                    )
                  ),
                TopicEnum.SCDC_STREET_LAMP_TOPIC
              )
            }

          }
        case _ =>
      }
    }

  }

}
