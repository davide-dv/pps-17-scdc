package controller.actors

import controller.actors.behaviours.ActorBehaviour
import controller.actors.utilities.TopicEnum
import controller.actors.utilities.TopicEnum.Topic
import model.components.actuators.Semaphore
import model.components.basic.Vehicle
import model.components.messages._
import model.components.sensors.Sensor
import play.api.libs.json.Json
import projectutilities.JsonUtilities

/**
  * Companion object for VehicleActor
  */
object VehicleActor {

  /**
    * Implicit and Helper for Vehicle
    *
    * @param vehicle the vehicle interested
    */
  implicit class VehicleActor(vehicle: Vehicle)
    extends ActorBehaviour
      with JsonUtilities {

    override def stateMsgAction(vehicleActor: FSMActor,
                                topic: Topic,
                                msg: GenericMessage): Unit = {

      topic match {
        case TopicEnum.SCDC_SENSOR_TOPIC =>
          vehicleActor.actorUtility.get
            .msgList(topic) = List()
        case TopicEnum.SCDC_SEMAPHORE_TOPIC =>
          if (vehicleActor.actorUtility.get
            .msgList(topic)
            .size >= vehicleActor.elements.count(
            e => e.isInstanceOf[Semaphore]
          )) {

            vehicleActor.actorUtility.get
              .msgList(topic) = List()

            vehicleActor.write(
              Json
                .toJson(
                  GenericMessage(
                    ElementInfo(
                      vehicle.id,
                      vehicle.direction
                    ),
                    vehicle.position,
                    messageTypology = MessageTypology.SYNCH_MSG
                  )
                ),
              TopicEnum.SCDC_VEHICLE_SYNCH_TOPIC
            )
          }
        case _ =>
      }

    }

    /**
      * Action when synch message is received
      *
      * @param vehicleActor the actor interested
      * @param topic        the topic where message is sent
      * @param msg          the message sent
      */
    def synchMsgAction(vehicleActor: FSMActor,
                       topic: Topic,
                       msg: GenericMessage): Unit = {

      topic match {
        case TopicEnum.SCDC_VEHICLE_SYNCH_TOPIC =>
          if (vehicleActor.actorUtility.get
            .msgList(topic)
            .size >= vehicleActor.elements.count(
            e => e.isInstanceOf[Vehicle]
          )) {
            vehicleActor.actorUtility.get.msgList(topic) = List()
            this.updateState(vehicleActor)
          }
        case _ =>
      }

    }

    /**
      * Updates vehicleActor's state
      *
      * @param vehicleActor the actor interested
      */
    def updateState(vehicleActor: FSMActor): Unit = {
      import controller.actors.behaviours.subjects.VehicleBehaviour._
      vehicle.move(vehicleActor.elements)

      val ev = EnvironmentMessage(
        Environments(
          crossRoadEnvironment = Option(
            CrossRoadEnvironment(
              vehicleActor.elements
                .filter(e => e.isInstanceOf[Semaphore])
                .asInstanceOf[List[Semaphore]],
              List(vehicle),
              vehicleActor.elements
                .filter(e => e.isInstanceOf[Sensor])
                .asInstanceOf[List[Sensor]]
            )
          )
        )
      )
      vehicleActor.write(
        Json.toJson(CommandMessage(Commands.UPDATE, Option(ev))),
        TopicEnum.SCDC_COMMAND_TOPIC
      )

    }
  }

}
