package controller.actors.utilities

/**
  * Enum for topics
  */
object TopicEnum extends Enumeration {
  type Topic = String
  val SCDC_VEHICLE_TOPIC = "scdc-vehicle-topic"
  val SCDC_SEMAPHORE_TOPIC = "scdc-semaphore-topic"
  val SCDC_SENSOR_TOPIC = "scdc-sensor-topic"
  val SCDC_REGISTER_TOPIC = "scdc-register-topic"
  val SCDC_COMMAND_TOPIC = "scdc-command-topic"
  val SCDC_STREET_LAMP_TOPIC = "scdc-street-lamp-topic"
  val SCDC_PERSON_TOPIC = "scdc-person-topic"
  val SCDC_SEMAPHORE_SYNCH_TOPIC = "scdc-semaphore-synch-topic"
  val SCDC_VEHICLE_SYNCH_TOPIC = "scdc-vehicle-synch-topic"
}
