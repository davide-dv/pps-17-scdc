package controller.actors.utilities

import controller.actors.utilities.TopicEnum.Topic
import controller.actors.{FSMActor, RegisterOperations}
import model.components.actuators.{Semaphore, SemaphoreInformation, StreetLamp}
import model.components.basic.{DirectionEnum, Person, Vehicle}
import model.components.messages.GenericMessage
import model.components.sensors.Sensor
import play.api.libs.json.Json
import projectutilities.JsonUtilities

import scala.collection.mutable

/**
  * Defines actor utilities
  */
trait FSMActorUtilities extends JsonUtilities {

  var FSMActor: FSMActor

  var msgList: mutable.Map[Topic, List[GenericMessage]]

  /**
    * Updates element position or state
    *
    * @param msg the message received
    */
  def updateElement(msg: GenericMessage): Unit = {
    this.FSMActor.elements
      .find(e => e.id == msg.elementInfo.id)
      .get match {
      case currentElem: Semaphore =>
        currentElem.semaphoreInformation_(
          Json.parse(msg.elementState).as[SemaphoreInformation]
        )
      case currentElem: Sensor     => currentElem.state_(msg.elementState)
      case currentElem: Vehicle    => currentElem.position_(msg.elementPosition)
      case currentElem: StreetLamp => currentElem.state_(msg.elementState)
      case currentElem: Person     => currentElem.position_(msg.elementPosition)
      case _                       => new IllegalArgumentException
    }
  }

  /**
    * Checks if a vehicle is out of the board
    *
    * @return true if a vehicle is out of bounds, false otherwise
    */
  def checkVehiclePosition(): Boolean = {
    if ((this.FSMActor.element.position.row < 0 && this.FSMActor.element.direction == DirectionEnum.NORTH)
        || (this.FSMActor.element.position.row > 7 && this.FSMActor.element.direction == DirectionEnum.SOUTH)
        || (this.FSMActor.element.position.column < 0 && this.FSMActor.element.direction == DirectionEnum.WEST)
        || (this.FSMActor.element.position.column > 7 && this.FSMActor.element.direction == DirectionEnum.EAST)) {
      this.FSMActor.actorRegisterOperation(RegisterOperations.LEFT)
      return true
    }
    false
  }

  /**
    * Checks if a person is out of the board
    *
    * @return true if a person is out of bounds, false otherwise
    */
  def checkPersonPosition(): Boolean = {
    if ((this.FSMActor.element.position.column < 0 && this.FSMActor.element.direction == DirectionEnum.WEST)
        || (this.FSMActor.element.position.column > 7 && this.FSMActor.element.direction == DirectionEnum.EAST)) {
      this.FSMActor.actorRegisterOperation(RegisterOperations.LEFT)
      return true
    }
    false
  }

}

/**
  * Companion object for actor utilities
  */
object FSMActorUtilities {

  /**
    * Creates an ActorUtilityImpl instance
    *
    * @param FSMActor the actor that send messages
    * @param msgList  map between topic and the related list of messages
    * @return a new instance
    */
  def apply(
    FSMActor: FSMActor,
    msgList: mutable.Map[Topic, List[GenericMessage]]
  ): FSMActorUtilities =
    FSMActorUtilitiesImpl(FSMActor, msgList)

  /**
    * Destroys an ActorUtilityImpl instance
    *
    * @param arg an ActorUtility instance
    * @return an Option of parameters
    */
  def unapply(
    arg: FSMActorUtilities
  ): Option[(FSMActor, mutable.Map[Topic, List[GenericMessage]])] =
    Some(arg.FSMActor, arg.msgList)

  /**
    * Instance for ActorUtilityImpl
    *
    * @param FSMActor the actor that send messages
    * @param msgList  map between topic and the related list of messages
    */
  case class FSMActorUtilitiesImpl(
    var FSMActor: FSMActor,
    var msgList: mutable.Map[Topic, List[GenericMessage]] =
      mutable.LinkedHashMap()
  ) extends FSMActorUtilities

}
