package controller.actors

import java.util.UUID.randomUUID

import projectutilities.{MessageUtilities, Observable}
import controller.actors.utilities.TopicEnum
import controller.actors.utilities.TopicEnum.Topic
import controller.basic.Scenario
import controller.basic.Scenario.Scenario
import model.components.actuators.{Semaphore, StreetLamp}
import model.components.basic.{Element, Person, Vehicle}
import model.components.messages._
import play.api.libs.json._

/**
  * Defines a SimulationActor
  */
trait SimulationActor extends Observable[SimulationActor] with MyActor {

  /**
    * Selected scenario
    *
    * @return the scenario
    */
  def scenario: Scenario

  /**
    * List of board's updated elements
    */
  var updatedElements: List[Element] = List()

  /**
    * Implementation of GeneralActor
    *
    * @return instance of GeneralActor
    */
  def generalActor: GeneralActor = new GeneralActorImpl(randomUUID().toString)

  /**
    * Start the simulation
    */
  def start(): Unit =
    this.write(
      Json.toJson(CommandMessage(Commands.START)),
      TopicEnum.SCDC_COMMAND_TOPIC
    )

  /**
    * Stop the simulation
    */
  def stop(): Unit =
    this.write(
      Json.toJson(CommandMessage(Commands.STOP)),
      TopicEnum.SCDC_COMMAND_TOPIC
    )

  override def init(): Unit = {
    generalActor.subscribe(
      List(TopicEnum.SCDC_COMMAND_TOPIC, TopicEnum.SCDC_REGISTER_TOPIC),
      read
    )
  }

  override def subscribe(topics: List[Topic]): Unit =
    new UnsupportedOperationException

  override def write(msg: JsValue, topic: Topic): Unit =
    generalActor.publish(msg, topic)

  var messages: List[EnvironmentMessage] = List()

  override def read(data: Data, topic: Topic): Unit = synchronized {
    topic match {
      case TopicEnum.SCDC_REGISTER_TOPIC =>
        val envMsg = data.msg.as[EnvironmentMessage]
        if (envMsg.msgOperation == RegisterOperations.NEW && envMsg.index >= this.currentIndex) {
          this.currentIndex = envMsg.index
          try {
            this.elements = MessageUtilities
              .updateEnvironment(this.elements, envMsg, scenario)
          } catch {
            case e: Exception => e.printStackTrace()
          }
        }
      case TopicEnum.SCDC_COMMAND_TOPIC =>
        try {
          val msg = data.msg.as[CommandMessage]
          if (msg.command == Commands.UPDATE) {
            this.messages = msg.environmentMessage.get :: this.messages
            scenario match {
              case Scenario.CROSS_ROAD =>
                if ((elements
                  .count(e => e.isInstanceOf[Vehicle]) == 0 && this.messages.size == elements
                  .count(e => e.isInstanceOf[Semaphore])) || (elements
                  .count(e => e.isInstanceOf[Vehicle]) != 0 && this.messages.size == elements
                  .count(e => e.isInstanceOf[Vehicle]))) {
                  val environment =
                    msg.environmentMessage.get.environments.crossRoadEnvironment.get
                  this.updatedElements = List()
                  if (elements.count(e => e.isInstanceOf[Vehicle]) != 0) {
                    this.updatedElements = environment.semaphores ::: environment.sensors
                    this.messages.foreach(msgEnv => {
                      this.updatedElements = msgEnv.environments.crossRoadEnvironment.get.vehicles.head :: this.updatedElements
                    })
                  } else {
                    this.updatedElements = environment.sensors
                    this.messages.foreach(msgEnv => {
                      this.updatedElements = msgEnv.environments.crossRoadEnvironment.get.semaphores.head :: this.updatedElements
                    })
                  }
                  this.messages = List()
                  notifyObservers()
                }
              case Scenario.SIDE_WALK =>
                if ((elements
                  .count(e => e.isInstanceOf[Person]) == 0 && this.messages.size == elements
                  .count(e => e.isInstanceOf[StreetLamp])) || (elements
                  .count(e => e.isInstanceOf[Person]) != 0 && this.messages.size == elements
                  .count(e => e.isInstanceOf[Person]))) {
                  val environment =
                    msg.environmentMessage.get.environments.sideWalkEnvironment.get
                  this.updatedElements = List()
                  this.updatedElements = environment.streetLamps ::: environment.sensors
                  if (elements.count(e => e.isInstanceOf[Person]) != 0) {
                    this.messages.foreach(msgEnv => {
                      this.updatedElements = msgEnv.environments.sideWalkEnvironment.get.people.head :: this.updatedElements
                    })
                  } else {
                    this.updatedElements = environment.sensors
                    this.messages.foreach(msgEnv => {
                      this.updatedElements = msgEnv.environments.sideWalkEnvironment.get.streetLamps.head :: this.updatedElements
                    })
                  }
                  this.messages = List()
                  notifyObservers()
                }
            }
          }
        } catch {
          case e: Exception => e.printStackTrace()
        }
    }
  }
}

/**
  * Companion object for SimulationActor
  */
object SimulationActor {

  /**
    * Creates a new instance of SimulationActorImpl
    *
    * @param elements list of board's element
    * @param scenario the chosen scenario
    * @return
    */
  def apply(elements: List[Element], scenario: Scenario): SimulationActor =
    new SimulationActorImpl(elements, scenario)

  /**
    * Instance of SimulationActorImpl
    *
    * @param simElements list of board's element
    * @param scenario    the chosen scenario
    */
  private class SimulationActorImpl(simElements: List[Element],
                                    override val scenario: Scenario)
    extends SimulationActor {
    this.elements = simElements
  }

}
