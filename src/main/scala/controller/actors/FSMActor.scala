package controller.actors

import java.util.{Timer, TimerTask}

import projectutilities.MessageUtilities
import controller.actors.RegisterOperations.RegisterOperation
import controller.actors.utilities.TopicEnum.Topic
import controller.actors.utilities.{FSMActorUtilities, TopicEnum}
import controller.basic.Scenario
import controller.basic.Scenario.Scenario
import model.components.actuators.{Semaphore, StreetLamp}
import model.components.basic._
import model.components.messages._
import model.components.sensors.Sensor
import play.api.libs.json._

import scala.collection.mutable

/**
  * Defines a FSMActor
  */
trait FSMActor extends MyActor {
  var element: Element

  var actorUtility: Option[FSMActorUtilities] = None

  val generalActor: GeneralActor = new GeneralActorImpl(element.id)

  override def read(data: Data, topic: Topic): Unit

  override def init(): Unit = {
    this.generalActor.subscribe(List(TopicEnum.SCDC_REGISTER_TOPIC), read)
    val timer = new Timer
    timer.schedule(new TimerTask {
      override def run(): Unit = {
        actorRegisterOperation(RegisterOperations.JOIN)
        timer.cancel()
      }
    }, 500)
  }

  override def write(msg: JsValue, topic: Topic): Unit = {
    this.generalActor.publish(msg, topic)
  }

  /**
    * Registers or unregisters an actor in the system
    *
    * @param operation the different possible operations
    */
  def actorRegisterOperation(operation: RegisterOperation): Unit = {
    try {
      this.generalActor
        .publish(
          Json.toJson(element match {
            case el: Sensor =>
              if (scenario == Scenario.SIDE_WALK)
                EnvironmentMessage(
                  Environments(
                    sideWalkEnvironment =
                      Option(SideWalkEnvironment(List(), List(), el :: List()))
                  ),
                  msgOperation = operation
                )
              else
                EnvironmentMessage(
                  Environments(
                    crossRoadEnvironment =
                      Option(CrossRoadEnvironment(List(), List(), el :: List()))
                  ),
                  msgOperation = operation
                )
            case el: Semaphore =>
              EnvironmentMessage(
                Environments(
                  crossRoadEnvironment =
                    Option(CrossRoadEnvironment(el :: List(), List(), List()))
                ),
                operation
              )
            case el: Vehicle =>
              EnvironmentMessage(
                Environments(
                  crossRoadEnvironment =
                    Option(CrossRoadEnvironment(List(), el :: List(), List()))
                ),
                operation
              )
            case el: Person =>
              EnvironmentMessage(
                Environments(
                  sideWalkEnvironment =
                    Option(SideWalkEnvironment(el :: List(), List(), List()))
                ),
                operation
              )
            case el: StreetLamp =>
              EnvironmentMessage(
                Environments(
                  sideWalkEnvironment =
                    Option(SideWalkEnvironment(List(), el :: List(), List()))
                ),
                operation
              )
            case _ =>
              if (scenario == Scenario.SIDE_WALK)
                EnvironmentMessage(
                  Environments(
                    sideWalkEnvironment =
                      Option(SideWalkEnvironment(List(), List(), List()))
                  ),
                  msgOperation = operation
                )
              else
                EnvironmentMessage(
                  Environments(
                    crossRoadEnvironment =
                      Option(CrossRoadEnvironment(List(), List(), List()))
                  ),
                  msgOperation = operation
                )
          }),
          TopicEnum.SCDC_REGISTER_TOPIC
        )
    } catch {
      case e: Exception => e.printStackTrace()
    }

    if (operation == RegisterOperations.LEFT) {
      this.generalActor.close()
    }
  }

  override def subscribe(topics: List[Topic]): Unit = {
    if (this.actorUtility.isEmpty) {
      val currentActorUtility = FSMActorUtilities(this, mutable.Map())
      topics.foreach(topic => {
        currentActorUtility.msgList = currentActorUtility.msgList + (topic -> List())
      })
      this.actorUtility = Option(currentActorUtility)
    } else {
      topics.foreach(topic => {
        this.actorUtility.get.msgList = this.actorUtility.get.msgList + (topic -> List())
      })
    }
    this.generalActor.subscribe(topics, read)
  }

  /**
    * Publishes the message on topic specified
    *
    * @param currentTopic   the topic
    * @param genericMessage the message
    */
  def start(currentTopic: TopicEnum.Topic,
            genericMessage: GenericMessage): Unit = {
    val timer = new Timer
    timer.schedule(new TimerTask {
      override def run(): Unit = {
        generalActor.publish(Json.toJson(genericMessage), currentTopic)
        timer.cancel()
      }
    }, 500)

  }

}

/**
  * Companion object for FSMActor
  */
object FSMActor {

  /**
    * Creates a FSMActor's instance
    *
    * @param e        the element
    * @param scenario the chosen scenario
    * @return new instance of FSMActorImpl
    */
  def apply(e: Element, scenario: Scenario): FSMActor =
    FSMActorImpl(e, scenario)

  /**
    * Instance of FSMActorImpl
    *
    * @param element  the element
    * @param scenario the chosen scenario
    */
  private case class FSMActorImpl(override var element: Element,
                                  var scenario: Scenario)
      extends FSMActor {

    override def read(data: Data, topic: Topic): Unit = synchronized {
      if (this.actorUtility.isDefined) {
        try {
          topic match {
            case TopicEnum.SCDC_REGISTER_TOPIC =>
              try {
                val envMsg = data.msg.as[EnvironmentMessage]

                if (envMsg.msgOperation == RegisterOperations.NEW && envMsg.index >= currentIndex) {
                  this.currentIndex = envMsg.index
                  this.elements = MessageUtilities
                    .updateEnvironment(this.elements, envMsg, this.scenario)
                }
              } catch {
                case e: Exception => e.printStackTrace();
              }
            case TopicEnum.SCDC_COMMAND_TOPIC =>
              val cmdMsg = data.msg.as[CommandMessage]
              cmdMsg.command match {
                case Commands.START =>
                  element match {
                    case el: Sensor =>
                      val timer = new Timer
                      timer.schedule(
                        new TimerTask {
                          override def run(): Unit = {
                            if (elements
                                  .count(e => e.isInstanceOf[Person]) == 0 && elements
                                  .count(e => e.isInstanceOf[Vehicle]) == 0) {
                              start(
                                TopicEnum.SCDC_SENSOR_TOPIC,
                                GenericMessage(
                                  ElementInfo(element.id, element.direction),
                                  element.position,
                                  el.state
                                )
                              )
                            }
                            timer.cancel()
                          }
                        },
                        500
                      )
                    case el: Person =>
                      if (!this.actorUtility.get.checkPersonPosition()) {
                        import controller.actors.behaviours.subjects.PersonBehaviour._
                        el.move()
                        this.start(
                          TopicEnum.SCDC_PERSON_TOPIC,
                          GenericMessage(
                            ElementInfo(
                              this.element.id,
                              this.element.direction
                            ),
                            this.element.position
                          )
                        )
                      }
                    case _: Vehicle =>
                      if (!this.actorUtility.get.checkVehiclePosition()) {
                        this.start(
                          TopicEnum.SCDC_VEHICLE_TOPIC,
                          GenericMessage(
                            ElementInfo(
                              this.element.id,
                              this.element.direction
                            ),
                            this.element.position
                          )
                        )
                      }
                    case _ =>
                  }
                case _ =>
              }
            case _ =>
              try {
                val msg = data.msg.as[GenericMessage]

                this.actorUtility.get.updateElement(msg)

                this.actorUtility.get
                  .msgList(topic) = msg :: this.actorUtility.get
                    .msgList(topic)

                msg.messageTypology match {
                  case MessageTypology.STATE_MSG =>
                    this.element match {
                      case el: Sensor =>
                        import SensorActor._
                        el.stateMsgAction(this, topic)
                      case el: Semaphore =>
                        import SemaphoreActor._
                        el.stateMsgAction(this, topic, msg)
                      case el: Vehicle =>
                        import VehicleActor._
                        el.stateMsgAction(this, topic, msg)
                      case el: Person =>
                        import PersonActor._
                        el.stateMsgAction(this, topic)
                      case el: StreetLamp =>
                        import StreetLampActor._
                        el.stateMsgAction(this, topic)
                      case _ => new IllegalArgumentException
                    }
                  case MessageTypology.SYNCH_MSG =>
                    this.element match {
                      case el: Semaphore =>
                        import SemaphoreActor._
                        el.synchMsgAction(this, topic, msg)
                      case el: Vehicle =>
                        import VehicleActor._
                        el.synchMsgAction(this, topic, msg)
                      case _ =>
                    }

                  case _ =>
                }

              } catch {
                case e: Exception => println(element.id + e.printStackTrace())
              }
          }
        } catch {
          case e: Exception => println(element.id + e.printStackTrace())
        }
      }
    }
  }

}
