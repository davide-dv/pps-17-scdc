package controller.actors.behaviours.sensors

import model.components.basic.{Element, LayerEnum}
import model.components.sensors.{Sensor, SensorEnum}

/**
  * Companion object for sensor behaviour
  */
object SensorBehaviour {

  /**
    * Implicit and helper for Sensor class
    *
    * @param s the used sensor
    */
  implicit class SensorHelper(s: Sensor) {

    /**
      * Main method that defines when a sensor should read a value
      *
      * @param elements list of board's elements
      */
    def readValue(elements: List[Element]): Unit =
      if (elements.exists(element => element.position == s.position &&
        (element.layer == LayerEnum.VEHICLE_LAYER || element.layer == LayerEnum.PERSON_LAYER))) {
        s.state_(SensorEnum.ON)
      } else {
        s.state_(SensorEnum.OFF)
      }
  }

}
