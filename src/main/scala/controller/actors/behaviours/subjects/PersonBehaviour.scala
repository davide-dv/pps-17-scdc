package controller.actors.behaviours.subjects

import model.components.basic._

/**
  * Companion object for person behaviour
  */
object PersonBehaviour {

  /**
    * Implicit and helper for Person class
    *
    * @param person the used person
    */
  implicit class PersonHelper(person: Person) {

    /**
      * Main method that defines when a person should make a move on the board
      *
      */
    def move(): Unit = {
      person.direction match {
        case DirectionEnum.EAST =>
          person.position_(
            Cell(person.position.column + 1, person.position.row)
          )
        case DirectionEnum.WEST =>
          person.position_(
            Cell(person.position.column - 1, person.position.row)
          )
        case _ =>
      }
    }
  }

}
