package controller.actors.behaviours.subjects

import model.components.actuators.{ColorEnum, Semaphore}
import model.components.basic._

/**
  * Companion object that defines a vehicle's behaviour
  */
object VehicleBehaviour {

  /**
    * Implicit and helper for Vehicle class
    *
    * @param vehicle the used vehicle
    */
  implicit class VehicleHelper(vehicle: Vehicle) {
    val offsetArray: Array[Int] = offsetMove(vehicle)

    /**
      * Main method that defines when a vehicle should make a move on the board
      *
      * @param elements list of board's elements
      * @param carFront true if there is a vehicle in front of the one given, false otherwise
      * @param semPres  true if there is a semaphore in front of the vehicle given, false otherwise
      * @param semGreen true if the semaphore in front of the vehicle given is green, false otherwise
      */
    def move(elements: List[Element],
             carFront: (Option[Element], Vehicle) => Boolean = isCarInFront,
             semPres: Option[Element] => Boolean = isSemaphorePresent,
             semGreen: (Option[Element], Vehicle) => Boolean = isSemaphoreGreen): Unit = {

      val forwardCar = getElementByLayer(elements, Cell(vehicle.position.column + offsetArray(0),
        vehicle.position.row + offsetArray(1)), LayerEnum.VEHICLE_LAYER)

      val semaphore = getElementByLayer(elements, Cell(vehicle.position.column, vehicle.position.row),
        LayerEnum.SEMAPHORE_LAYER)

      if (!carFront(forwardCar, vehicle) && (!semPres(semaphore) ||
        (semPres(semaphore) && semGreen(semaphore, vehicle)))) {
        if (vehicle.direction.equals(DirectionEnum.SOUTH) &&
          vehicle.position.equals(Cell(vehicle.position.column, 7))) {
          vehicle.position_(Cell(vehicle.position.column + offsetArray(0),
            vehicle.position.row + offsetArray(1) + 1))
        } else {
          vehicle.position_(Cell(vehicle.position.column + offsetArray(0),
            vehicle.position.row + offsetArray(1)))
        }
      }
    }

    /**
      * Getter for an element with given parameters
      *
      * @param elements    list of board's elements
      * @param compareCell front cell of vehicle used
      * @param layer       the layer i want to explore
      * @return Some(element) if present, None otherwise
      */
    private def getElementByLayer(elements: List[Element], compareCell: Cell, layer: LayerEnum.Layer): Option[Element] =
      elements find (e => e.layer == layer && e.position.equals(compareCell))

    /**
      * Defines vehicle's step on the board considering its direction
      *
      * @param vehicle the given vehicle
      * @return an array of size 2, in position 0 there is column step, in position 1 row step
      */
    private def offsetMove(vehicle: Vehicle): Array[Int] = vehicle.direction match {
      case DirectionEnum.WEST => Array(-1, 0)
      case DirectionEnum.EAST => Array(1, 0)
      case DirectionEnum.NORTH => Array(0, -1)
      case DirectionEnum.SOUTH => Array(0, 1)
    }

    /**
      * Verifies if a semaphore is present in front of vehicle analyzed
      *
      * @param forwardSemaphore an Option that represent forward semaphore
      * @return true if is present, false if is None
      */
    private def isSemaphorePresent(forwardSemaphore: Option[Element]): Boolean = forwardSemaphore.isDefined

    /**
      * Verifies if semaphore in front of vehicle is green
      *
      * @param forwardSemaphore an Option that represent forward semaphore
      * @param vehicle          the vehicle given
      * @return true if semaphore is green, false otherwise
      */
    private def isSemaphoreGreen(forwardSemaphore: Option[Element], vehicle: Vehicle): Boolean = forwardSemaphore match {
      case _ if forwardSemaphore.get.isInstanceOf[Semaphore] &&
        forwardSemaphore.get.asInstanceOf[Semaphore].semaphoreInformation.state == ColorEnum.GREEN => true
      case _ => false
    }

    /**
      * Verifies if another vehicle is in front of given vehicle
      *
      * @param forwardCar an Option that represents forward vehicle
      * @param vehicle    the vehicle given
      * @return true is a vehicle is present, false otherwise
      */
    private def isCarInFront(forwardCar: Option[Element], vehicle: Vehicle): Boolean = forwardCar match {
      case None => false
      case _ if forwardCar.get.position.equals(Cell(vehicle.position.column + offsetArray(0),
        vehicle.position.row + offsetArray(1))) => true
      case _ => false
    }
  }

}
