package controller.actors.behaviours.actuators

import model.components.actuators.{ColorEnum, Semaphore, SemaphoreInformation}
import model.components.basic.{DirectionEnum, Element}
import model.components.sensors.{Sensor, SensorEnum}

import scala.language.postfixOps

/**
  * Companion object for semaphore behaviour
  */
object SemaphoreBehaviour {

  /**
    * Implicit and helper for Semaphore class
    *
    * @param s the used semaphore
    */
  implicit class SemaphoreHelper(s: Semaphore) {

    val greenMaxTimeMillis = 3000
    val yellowMaxTimeMillis = 1000
    val redMaxTimeMillis = 3000
    val awaitTime = 15000

    /**
      * Set the semaphore that is waiting for most
      *
      * @param semaphores the semaphores' list
      */
    def setLongAwaitedSemaphore(semaphores: List[Semaphore]): Unit = {

      /**
        * Getter for semaphore that is waiting for most
        *
        * @param semaphores list of semaphores
        * @return long-awaited semaphore
        */
      def getLongAwaitedSemaphore(semaphores: List[Semaphore]): Semaphore = {
        var semaphore: Semaphore = semaphores.head
        semaphores.foreach(sem => {
          if (offset(sem.semaphoreInformation.lastChangeStateTime) > offset(
                semaphore.semaphoreInformation.lastChangeStateTime
              )) {
            semaphore = sem
          }
        })
        semaphore
      }

      s.longAwaitedSemaphore = Some(getLongAwaitedSemaphore(semaphores))

    }

    /**
      * Calculates time offset from a given start time
      *
      * @param startTime start time that i want to calculate offset
      * @return long number, difference between start time and current time
      */
    def offset(startTime: Long): Long = System.currentTimeMillis() - startTime

    /**
      * Main method that defines when a semaphore should change state
      *
      * @param elements the list of element on the board
      * @return the color assumed by semaphore
      */
    def nextState(elements: List[Element]): ColorEnum.Color = {
      val semaphores: List[Semaphore] = elements
        .filter(e => e.isInstanceOf[Semaphore])
        .sortWith(
          (e1, e2) =>
            e1.asInstanceOf[Semaphore]
              .semaphoreInformation
              .lastWaitingDetectedCar >
              e2.asInstanceOf[Semaphore]
                .semaphoreInformation
                .lastWaitingDetectedCar
        )
        .map(s => s.asInstanceOf[Semaphore])

      /**
        * Getter for semaphore with given direction
        *
        * @param semaphores list of board's semaphores
        * @param direction  the direction interested
        * @return interested semaphore
        */
      def getSemaphoreByDirection(
        semaphores: List[Semaphore],
        direction: DirectionEnum.Direction
      ): Semaphore = {
        semaphores find (s => s.direction == direction) get
      }

      /**
        * Semaphore behaviour with red state, if a semaphore is waiting for most redBehaviour() is called,
        * red color otherwise; it is also called when a vehicle is waiting for most between all semaphores,
        * red color otherwise
        *
        * @param semaphores list of semaphores
        * @param direction  direction of semaphore interested
        * @return color that semaphore will assume
        */
      def redStatusSemaphoreBehaviour(
        semaphores: List[Semaphore],
        direction: DirectionEnum.Direction
      ): ColorEnum.Color = {
        if (offset(
              s.longAwaitedSemaphore.get.semaphoreInformation.lastChangeStateTime
            ) > this.awaitTime) {
          if (s.id == s.longAwaitedSemaphore.get.id) {
            return redBehaviour()
          } else {
            return ColorEnum.RED
          }
        }
        if (s.semaphoreInformation.lastWaitingDetectedCar >= semaphores.head.semaphoreInformation.lastWaitingDetectedCar) {
          return redBehaviour()
        }

        /**
          * Defines semaphore behaviour when its color is red, if semaphore is in North or South it checks if
          * semaphores on West and East direction are red, if true semaphore's color will be green, red otherwise.
          * Opposite thinking for West and East semaphore's direction
          *
          * @return color that semaphore should assume
          */
        def redBehaviour(): ColorEnum.Color = {
          s.direction match {
            case DirectionEnum.NORTH | DirectionEnum.SOUTH =>
              if (getSemaphoreByDirection(semaphores, DirectionEnum.WEST).semaphoreInformation.state == ColorEnum.RED && getSemaphoreByDirection(
                    semaphores,
                    DirectionEnum.EAST
                  ).semaphoreInformation.state == ColorEnum.RED) {
                ColorEnum.GREEN
              } else {
                ColorEnum.RED
              }
            case DirectionEnum.EAST | DirectionEnum.WEST =>
              if (getSemaphoreByDirection(semaphores, DirectionEnum.NORTH).semaphoreInformation.state == ColorEnum.RED && getSemaphoreByDirection(
                    semaphores,
                    DirectionEnum.SOUTH
                  ).semaphoreInformation.state == ColorEnum.RED) {
                ColorEnum.GREEN
              } else {
                ColorEnum.RED
              }
            case _ => ColorEnum.RED
          }
        }

        ColorEnum.RED
      }

      var color: ColorEnum.Color = s.semaphoreInformation.state

      s.semaphoreInformation.state match {
        case ColorEnum.GREEN =>
          if (offset(s.semaphoreInformation.lastChangeStateTime) > greenMaxTimeMillis) {
            color = ColorEnum.YELLOW
          }
        case ColorEnum.YELLOW =>
          if (offset(s.semaphoreInformation.lastChangeStateTime) > yellowMaxTimeMillis) {
            color = ColorEnum.RED
          }
        case ColorEnum.RED =>
          s.direction match {
            case DirectionEnum.NORTH =>
              if (getSemaphoreByDirection(semaphores, DirectionEnum.SOUTH).semaphoreInformation.state == ColorEnum.GREEN) {
                color = ColorEnum.GREEN
              }
            case DirectionEnum.SOUTH =>
              if (getSemaphoreByDirection(semaphores, DirectionEnum.NORTH).semaphoreInformation.state == ColorEnum.GREEN) {
                color = ColorEnum.GREEN
              }
            case DirectionEnum.EAST =>
              if (getSemaphoreByDirection(semaphores, DirectionEnum.WEST).semaphoreInformation.state == ColorEnum.GREEN) {
                color = ColorEnum.GREEN
              }
            case DirectionEnum.WEST =>
              if (getSemaphoreByDirection(semaphores, DirectionEnum.EAST).semaphoreInformation.state == ColorEnum.GREEN) {
                color = ColorEnum.GREEN
              }
            case _ =>
          }

          if (offset(s.semaphoreInformation.lastChangeStateTime) > redMaxTimeMillis && color == ColorEnum.RED) {
            s.direction match {
              case DirectionEnum.NORTH =>
                color =
                  redStatusSemaphoreBehaviour(semaphores, DirectionEnum.SOUTH)
              case DirectionEnum.SOUTH =>
                color =
                  redStatusSemaphoreBehaviour(semaphores, DirectionEnum.NORTH)
              case DirectionEnum.EAST =>
                color =
                  redStatusSemaphoreBehaviour(semaphores, DirectionEnum.WEST)
              case DirectionEnum.WEST =>
                color =
                  redStatusSemaphoreBehaviour(semaphores, DirectionEnum.EAST)
              case _ =>
            }
          }

      }
      if (color != s.semaphoreInformation.state) {
        s.semaphoreInformation_(SemaphoreInformation(color))
      }
      s.semaphoreInformation.state
    }

    /**
      * Updates waiting car on semaphore information
      *
      * @param elements list of board's elements
      */
    def updateWaitingCar(elements: List[Element]): Unit = {

      val sensors: List[Sensor] = elements
        .filter(e => e.isInstanceOf[Sensor])
        .map(s => s.asInstanceOf[Sensor])

      def countWaitingCar(sensors: List[Sensor],
                          direction: DirectionEnum.Direction): Long = {
        sensors.count(
          s => s.direction == direction && s.state == SensorEnum.ON
        )
      }

      s.semaphoreInformation.lastWaitingDetectedCar =
        countWaitingCar(sensors, s.direction)

    }

  }

}
