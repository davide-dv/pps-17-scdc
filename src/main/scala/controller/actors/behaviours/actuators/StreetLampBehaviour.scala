package controller.actors.behaviours.actuators

import model.components.actuators.{StreetLamp, StreetLampStates}
import model.components.basic.{Cell, Element, LayerEnum}
import model.components.sensors.{Sensor, SensorEnum}

/**
  * Companion object for street lamp behaviour
  */
object StreetLampBehaviour {

  /**
    * Implicit and helper for Street Lamp class
    *
    * @param s the used street lamp
    */
  implicit class StreetLampHelper(s: StreetLamp) {

    /**
      * Main method that defines when a street lamp should change state
      *
      * @param elements list of board's elements
      */
    def nextState(elements: List[Element]): Unit = {

      /**
        * Verifies sensors' state from list of sensors
        *
        * @param sensors        the list of sensors
        * @param stateStrategy the sensor's state i
        * @return true if state changes, false otherwise
        */
      def checkSensorState(
        sensors: List[Option[Element]],
        stateStrategy: StreetLampStates.StreetLampState
      ): Boolean = sensors match {
        //This case matches the case when the list is empty
        case Nil => false
        case _ =>
          sensors.head.getOrElse(None) match {
            //This is the case when the sensor's position is out of bounds. e.g. Cell(-1,0) or Cell(9,1)
            case None =>
              checkSensorState(
                sensors.filter(sensor => sensor != sensors.head),
                StreetLampStates.MEDIUM
              )
            //This case checks the sensor state
            case sensor: Sensor =>
              if (sensor.state == SensorEnum.ON) {
                s.state_(stateStrategy)
                return true
              }
              checkSensorState(
                sensors.filter(sensor => sensor != sensors.head),
                StreetLampStates.MEDIUM
              )
            case _ => false
          }
      }

      if (checkSensorState(
            List(
              getElementByLayer(
                elements,
                Cell(s.position.column, 1),
                LayerEnum.SENSOR_LAYER
              ),
              getElementByLayer(
                elements,
                Cell(s.position.column - 1, 1),
                LayerEnum.SENSOR_LAYER
              ),
              getElementByLayer(
                elements,
                Cell(s.position.column + 1, 1),
                LayerEnum.SENSOR_LAYER
              )
            ),
            StreetLampStates.HIGH
          )) return

      s.state_(StreetLampStates.LOW)
    }

    /**
      * Getter for an element with given parameters
      *
      * @param elements    list of board's elements
      * @param compareCell front cell of vehicle used
      * @param layer       the layer i want to explore
      * @return Some(element) if present, None otherwise
      */
    private def getElementByLayer(elements: List[Element],
                                  compareCell: Cell,
                                  layer: LayerEnum.Layer): Option[Element] =
      elements find (e => e.layer == layer && e.position.equals(compareCell))

  }

}
