package controller.actors.behaviours

import controller.actors.FSMActor
import controller.actors.utilities.TopicEnum.Topic
import model.components.messages.GenericMessage

/**
  * Defines actor behaviour
  */
trait ActorBehaviour {

  /**
    * Action on state message reception
    *
    * @param actor actor that send on the topic
    * @param topic topic where send message
    * @param msg   message to be sent
    */
  def stateMsgAction(actor: FSMActor, topic: Topic, msg: GenericMessage = null)
}
