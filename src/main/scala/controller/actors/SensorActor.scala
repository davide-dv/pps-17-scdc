package controller.actors

import projectutilities.JsonUtilities
import controller.actors.behaviours.ActorBehaviour
import controller.actors.utilities.TopicEnum
import controller.actors.utilities.TopicEnum.Topic
import model.components.basic.LayerEnum
import model.components.basic.LayerEnum.Layer
import model.components.messages.{ElementInfo, GenericMessage}
import model.components.sensors.Sensor
import play.api.libs.json.Json

/**
  * Companion object for SensorActor
  */
object SensorActor {

  /**
    * Implicit and Helper for Sensor
    *
    * @param sensor the sensor interested
    */
  implicit class SensorActor(sensor: Sensor)
    extends ActorBehaviour
      with JsonUtilities {

    override def stateMsgAction(sensorActor: FSMActor,
                                topic: Topic,
                                msg: GenericMessage): Unit = {

      topic match {
        case TopicEnum.SCDC_VEHICLE_TOPIC | TopicEnum.SCDC_PERSON_TOPIC =>
          val currentLayer: Layer =
            if (topic == TopicEnum.SCDC_VEHICLE_TOPIC) LayerEnum.VEHICLE_LAYER
            else LayerEnum.PERSON_LAYER
          if (sensorActor.actorUtility.get
            .msgList(topic)
            .size >= sensorActor.elements
            .count(e => e.layer == currentLayer)) {

            import controller.actors.behaviours.sensors.SensorBehaviour._
            sensor.readValue(sensorActor.elements)
            sensorActor.actorUtility.get.msgList(topic) = List()
            sensorActor.write(
              Json
                .toJson(
                  GenericMessage(
                    ElementInfo(
                      sensor.id,
                      sensor.direction
                    ),
                    sensor.position,
                    sensor.state
                  )
                ),
              TopicEnum.SCDC_SENSOR_TOPIC
            )
          }
        case _ =>
      }

    }

  }

}
