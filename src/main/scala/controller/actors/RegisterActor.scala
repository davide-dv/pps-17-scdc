package controller.actors

import java.util.UUID.randomUUID

import controller.actors.utilities.TopicEnum
import controller.actors.utilities.TopicEnum.Topic
import controller.basic.Scenario
import controller.basic.Scenario.Scenario
import model.components.actuators.{Semaphore, StreetLamp}
import model.components.basic.{Person, Vehicle}
import model.components.messages._
import model.components.sensors.Sensor
import play.api.libs.json._


/**
  * Enum for RegisterOperations
  */
object RegisterOperations extends Enumeration {
  type RegisterOperation = String
  val LEFT = "left"
  val JOIN = "join"
  val NEW = "new"
}

/**
  * Defines a RegisterActor
  */
trait RegisterActor extends MyActor

/**
  * Object for RegisterActor
  */
object RegisterActor {

  /**
    * Creates a new instance of RegisterActorImpl
    *
    * @param scenario the chosen scenario
    * @return a new instance of RegisterActorImpl
    */
  def apply(scenario: Scenario): RegisterActor = new RegisterActorImpl(scenario)

  /**
    * Class for RegisterActorImpl
    *
    * @param scenario the chosen scenario
    */
  class RegisterActorImpl(var scenario: Scenario) extends RegisterActor {

    private val registerActor: GeneralActor = new GeneralActorImpl(
      randomUUID().toString
    )

    override def read(data: Data, topic: Topic): Unit = synchronized {
      val environmentMessage = data.msg.as[EnvironmentMessage]
      environmentMessage.msgOperation match {
        case RegisterOperations.JOIN =>
          scenario match {
            case Scenario.SIDE_WALK =>
              val environment =
                environmentMessage.environments.sideWalkEnvironment.get
              environment.streetLamps
                .foreach(sl => {
                  this.elements = sl :: this.elements
                })
              environment.people.foreach(p => {
                this.elements = p :: this.elements
              })
              environment.sensors.foreach(s => {
                this.elements = s :: this.elements
              })
            case Scenario.CROSS_ROAD =>
              val environment =
                environmentMessage.environments.crossRoadEnvironment.get
              environment.vehicles.foreach(v => {
                this.elements = v :: this.elements
              })
              environment.semaphores.foreach(se => {
                this.elements = se :: this.elements
              })
              environment.sensors.foreach(s => {
                this.elements = s :: this.elements
              })
            case _ =>
          }

          sendNewEnvironment()
        case RegisterOperations.LEFT =>
          scenario match {
            case Scenario.SIDE_WALK =>
              val environment =
                environmentMessage.environments.sideWalkEnvironment.get
              environment.streetLamps.foreach(v => {
                this.elements = this.elements.filter(_.id != v.id)
              })
              environment.people.foreach(p => {
                this.elements = this.elements.filter(_.id != p.id)
              })
              environment.sensors.foreach(s => {
                this.elements = this.elements.filter(_.id != s.id)
              })
            case Scenario.CROSS_ROAD =>
              val environment =
                environmentMessage.environments.crossRoadEnvironment.get
              environment.vehicles.foreach(v => {
                this.elements = this.elements.filter(_.id != v.id)
              })
              environment.semaphores.foreach(se => {
                this.elements = this.elements.filter(_.id != se.id)
              })
              environment.sensors.foreach(s => {
                this.elements = this.elements.filter(_.id != s.id)
              })
            case _ =>
          }
          sendNewEnvironment()
        case _ =>
      }
    }

    override def write(msg: JsValue, topic: Topic): Unit = {
      this.registerActor.publish(msg, topic)
    }

    override def subscribe(topics: List[Topic]): Unit = {
      this.registerActor.subscribe(topics, read)
    }

    override def init(): Unit = {
      this.subscribe(List(TopicEnum.SCDC_REGISTER_TOPIC))
    }

    /**
      * Sends new environment message
      */
    def sendNewEnvironment(): Unit = synchronized {
      this.currentIndex += 1
      val environmentMessage =
        if (scenario == Scenario.SIDE_WALK)
          EnvironmentMessage(
            Environments(
              sideWalkEnvironment = Option(
                SideWalkEnvironment(
                  this.elements
                    .filter(e => e.isInstanceOf[Person])
                    .asInstanceOf[List[Person]],
                  this.elements
                    .filter(e => e.isInstanceOf[StreetLamp])
                    .asInstanceOf[List[StreetLamp]],
                  this.elements
                    .filter(e => e.isInstanceOf[Sensor])
                    .asInstanceOf[List[Sensor]]
                )
              )
            ),
            RegisterOperations.NEW,
            this.currentIndex
          )
        else
          EnvironmentMessage(
            Environments(
              crossRoadEnvironment = Option(
                CrossRoadEnvironment(
                  this.elements
                    .filter(e => e.isInstanceOf[Semaphore])
                    .asInstanceOf[List[Semaphore]],
                  this.elements
                    .filter(e => e.isInstanceOf[Vehicle])
                    .asInstanceOf[List[Vehicle]],
                  this.elements
                    .filter(e => e.isInstanceOf[Sensor])
                    .asInstanceOf[List[Sensor]]
                )
              )
            ),
            RegisterOperations.NEW,
            this.currentIndex
          )
      write(Json.toJson(environmentMessage), TopicEnum.SCDC_REGISTER_TOPIC)
    }

  }

}

