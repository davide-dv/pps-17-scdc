package controller.actors

import projectutilities.JsonUtilities
import controller.actors.utilities.TopicEnum.Topic
import controller.basic.Scenario.Scenario
import model.components.basic.Element
import play.api.libs.json.JsValue

trait MyActor extends JsonUtilities {

  /**
    * List of board's elements
    */
  var elements: List[Element] = List()

  /**
    * Index to find elements in list
    */
  var currentIndex = 0

  /**
    * Initialize actor
    */
  def init()

  /**
    * The chosen scenario
    *
    * @return the scenario
    */
  def scenario: Scenario

  /**
    * Subscribe actors to a list of topics
    *
    * @param topics the topics' list
    */
  def subscribe(topics: List[Topic]): Unit

  /**
    * Writes a message to a topic
    *
    * @param msg   the message in format JsValue
    * @param topic the topic
    */
  def write(msg: JsValue, topic: Topic)

  /**
    * Reads a data from a certain topic
    *
    * @param data  the data
    * @param topic to topic
    */
  def read(data: Data, topic: Topic): Unit

}
