package controller.actors

import controller.actors.behaviours.ActorBehaviour
import controller.actors.utilities.TopicEnum
import controller.actors.utilities.TopicEnum.Topic
import model.components.actuators.Semaphore
import model.components.basic.{DirectionEnum, Vehicle}
import model.components.messages._
import model.components.sensors.Sensor
import play.api.libs.json.Json
import projectutilities.JsonUtilities

/**
  * Companion object for SemaphoreActor
  */
object SemaphoreActor {

  /**
    * mplicit and Helper for Semaphore
    *
    * @param semaphore the used semaphore
    */
  implicit class SemaphoreActor(semaphore: Semaphore)
      extends ActorBehaviour
      with JsonUtilities {

    /**
      * Send message with semaphore information
      *
      * @param semaphoreActor the actor that send message
      * @param topic          the topic on where to send message
      * @param msgTypology    the message typology
      */
    def sendSemaphoreInfo(semaphoreActor: FSMActor,
                          topic: Topic,
                          msgTypology: MessageTypology.Type): Unit = {
      semaphoreActor.write(
        Json
          .toJson(
            GenericMessage(
              ElementInfo(semaphore.id, semaphore.direction),
              semaphore.position,
              Json
                .stringify(Json.toJson(semaphore.semaphoreInformation)),
              messageTypology = msgTypology
            )
          ),
        topic
      )
    }

    override def stateMsgAction(semaphoreActor: FSMActor,
                                topic: Topic,
                                msg: GenericMessage): Unit = {

      topic match {
        case TopicEnum.SCDC_SENSOR_TOPIC =>
          if (semaphoreActor.actorUtility.get
                .msgList(topic)
                .size >= semaphoreActor.elements.count(
                e => e.isInstanceOf[Sensor]
              )) {
            try {
              semaphoreActor.actorUtility.get.msgList(topic) = List()

              import controller.actors.behaviours.actuators.SemaphoreBehaviour._
              semaphore.updateWaitingCar(semaphoreActor.elements)
              this.sendSemaphoreInfo(
                semaphoreActor,
                TopicEnum.SCDC_SEMAPHORE_SYNCH_TOPIC,
                MessageTypology.SYNCH_MSG
              )

            } catch {
              case e: Exception => e.printStackTrace()
            }
          }
        case TopicEnum.SCDC_SEMAPHORE_SYNCH_TOPIC =>
          semaphoreActor.actorUtility.get.msgList(topic) = List()
          (semaphore.direction, msg.elementInfo.direction) match {
            case (DirectionEnum.EAST, DirectionEnum.NORTH) =>
              this.updateState(semaphoreActor)
            case (DirectionEnum.SOUTH, DirectionEnum.EAST) =>
              this.updateState(semaphoreActor)
            case (DirectionEnum.WEST, DirectionEnum.SOUTH) =>
              this.updateState(semaphoreActor)
            case _ =>
          }
        case _ =>
      }

    }

    /**
      * Action when synch message is received
      *
      * @param semaphoreActor the actor interested
      * @param topic          the topic where message is sent
      * @param msg            the message sent
      */
    def synchMsgAction(semaphoreActor: FSMActor,
                       topic: Topic,
                       msg: GenericMessage): Unit = {

      topic match {
        case TopicEnum.SCDC_SEMAPHORE_SYNCH_TOPIC =>
          if (semaphoreActor.actorUtility.get
                .msgList(topic)
                .size >= semaphoreActor.elements.count(
                e => e.isInstanceOf[Semaphore]
              )) {
            semaphoreActor.actorUtility.get.msgList(topic) = List()
            import controller.actors.behaviours.actuators.SemaphoreBehaviour._
            semaphore.setLongAwaitedSemaphore(
              semaphoreActor.elements
                .filter(e => e.isInstanceOf[Semaphore])
                .map(el => el.asInstanceOf[Semaphore])
            )
            if (semaphore.direction == DirectionEnum.NORTH) {
              this.updateState(semaphoreActor)
            }
          }
        case _ =>
      }

    }

    /**
      * Updates vehicleActor's state
      *
      * @param semaphoreActor the actor interested
      */
    def updateState(semaphoreActor: FSMActor): Unit = {
      import controller.actors.behaviours.actuators.SemaphoreBehaviour._
      semaphore.nextState(semaphoreActor.elements)
      this.sendSemaphoreInfo(
        semaphoreActor,
        TopicEnum.SCDC_SEMAPHORE_SYNCH_TOPIC,
        MessageTypology.STATE_MSG
      )
      if (semaphoreActor.elements.count(el => el.isInstanceOf[Vehicle]) == 0) {
        val ev = EnvironmentMessage(
          Environments(
            crossRoadEnvironment = Option(
              CrossRoadEnvironment(
                List(semaphore),
                List(),
                semaphoreActor.elements
                  .filter(e => e.isInstanceOf[Sensor])
                  .asInstanceOf[List[Sensor]]
              )
            )
          )
        )
        semaphoreActor.write(
          Json.toJson(CommandMessage(Commands.UPDATE, Option(ev))),
          TopicEnum.SCDC_COMMAND_TOPIC
        )
      } else {
        this.sendSemaphoreInfo(
          semaphoreActor,
          TopicEnum.SCDC_SEMAPHORE_TOPIC,
          MessageTypology.STATE_MSG
        )
      }

    }
  }

}
