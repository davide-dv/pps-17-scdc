package controller.actors

import projectutilities.JsonUtilities
import controller.actors.behaviours.ActorBehaviour
import controller.actors.utilities.TopicEnum
import controller.actors.utilities.TopicEnum.Topic
import model.components.actuators.StreetLamp
import model.components.basic.Person
import model.components.messages._
import model.components.sensors.Sensor
import play.api.libs.json.Json

/**
  * Companion object for PersonActor
  */
object PersonActor {

  /**
    * Implicit and Helper for PersonActor
    *
    * @param person the person to apply stateMsgAction
    */
  implicit class PersonActor(person: Person)
    extends ActorBehaviour
      with JsonUtilities {

    override def stateMsgAction(personActor: FSMActor,
                                topic: Topic,
                                msg: GenericMessage): Unit = {

      topic match {
        case TopicEnum.SCDC_SENSOR_TOPIC =>
          personActor.actorUtility.get.msgList(topic) = List()
        case TopicEnum.SCDC_STREET_LAMP_TOPIC =>
          if (personActor.actorUtility.get
            .msgList(topic)
            .size >= personActor.elements
            .count(e => e.isInstanceOf[StreetLamp])) {
            personActor.actorUtility.get.msgList(topic) = List()

            val ev = EnvironmentMessage(
              Environments(
                sideWalkEnvironment = Option(
                  SideWalkEnvironment(
                    List(person),
                    personActor.elements
                      .filter(e => e.isInstanceOf[StreetLamp])
                      .asInstanceOf[List[StreetLamp]],
                    personActor.elements
                      .filter(e => e.isInstanceOf[Sensor])
                      .asInstanceOf[List[Sensor]]
                  )
                )
              )
            )

            personActor.write(
              Json.toJson(CommandMessage(Commands.UPDATE, Option(ev))),
              TopicEnum.SCDC_COMMAND_TOPIC
            )

          }
        case _ =>
      }

    }

  }

}
