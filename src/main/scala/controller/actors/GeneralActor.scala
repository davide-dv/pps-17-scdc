package controller.actors

import akka.actor.{ActorRef, ActorSystem, Props}
import com.spingo.op_rabbit._
import controller.actors.utilities.TopicEnum.Topic
import play.api.libs.json.{JsValue, Json, OFormat}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

/**
  * Defines a GeneralActor
  */
trait GeneralActor {

  /**
    * Subscribes the actor to a list of topic applying callback
    *
    * @param topics   the list of topics
    * @param callback the function for subscribe
    */
  def subscribe(topics: List[String], callback: (Data, Topic) => Unit): Unit

  /**
    * Publishes a message to a topic
    *
    * @param msg   the message
    * @param topic the topic
    */
  def publish(msg: JsValue, topic: Topic)

  /**
    * Close the actor subscriptions
    */
  def close()
}

/**
  * Class for data in JsValue format
  *
  * @param msg the message in JsValue format
  */
case class Data(msg: JsValue)

/**
  * Instance of GeneralActorImpl
  *
  * @param id the actor's id
  */
class GeneralActorImpl(id: String) extends GeneralActor {

  import PlayJsonSupport._

  private val SYSTEM_NAME = "scdc-actor-system"

  implicit private val actorSystem: ActorSystem = ActorSystem(SYSTEM_NAME)
  implicit private val dataFormat: OFormat[Data] = Json.format[Data]

  private val actor: ActorRef =
    this.actorSystem.actorOf(Props(new RabbitControl), "actor-" + id)
  implicit private val recoveryStrategy: RecoveryStrategy =
    RecoveryStrategy.limitedRedeliver(1.second, 10)

  var subscriptionRefs: List[SubscriptionRef] = List()

  import ExecutionContext.Implicits.global

  override def subscribe(topics: List[Topic],
                         callback: (Data, Topic) => Unit): Unit = {
    this.subscriptionRefs = Subscription.run(actor) {
      import Directives._
      channel(qos = 12) {
        consume(topic(queue(s"actor-$id-queue", autoDelete = true), topics)) {
          (body(as[Data]) & routingKey) { (data, topic) =>
            ack(Future {
              callback(data, topic)
            })
          }
        }
      }
    } :: this.subscriptionRefs
  }

  override def publish(msg: JsValue, topic: Topic) {
    this.actor ! Message.topic(Data(msg), topic)
  }

  override def close(): Unit = {
    this.subscriptionRefs.foreach(subscriptionRef => {
      subscriptionRef.abort()
    })
    this.subscriptionRefs = List()
  }

}
