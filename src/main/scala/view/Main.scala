package view

import projectutilities.ViewUtilities
import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.{Stage, WindowEvent}

/**
  * Main class
  */
class Main extends Application {

  /**
    * Method to start the stage
    *
    * @param primaryStage the stage to be started
    */
  override def start(primaryStage: Stage) {
    val scene = new Scene(Menu(primaryStage))
    scene.getStylesheets.add(ViewUtilities.MENU_CSS_PATH)
    primaryStage.getIcons.add(
      new Image(getClass.getResource(ViewUtilities.CAR_ICON_PATH).toString)
    )
    primaryStage.setResizable(false)
    primaryStage.setOnCloseRequest((_: WindowEvent) => System.exit(0))
    primaryStage.setTitle(ViewUtilities.MENU_TITLE)
    primaryStage.setScene(scene)
    primaryStage.show()
  }
}

/**
  * Main object
  */
object Main {
  def main(args: Array[String]) {
    import java.util.Locale
    Locale.setDefault(Locale.ENGLISH)
    Application.launch(classOf[Main], args: _*)
  }
}
