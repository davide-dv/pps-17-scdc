package view

import java.util.Timer

import controller.basic.Scenario
import projectutilities.ViewUtilities
import javafx.application.Platform
import javafx.collections.{FXCollections, ObservableList}
import javafx.event.ActionEvent
import javafx.fxml.FXMLLoader
import javafx.geometry.{Insets, Pos}
import javafx.scene.control.Alert.AlertType
import javafx.scene.control._
import javafx.scene.image.{Image, ImageView}
import javafx.scene.layout.{BorderPane, HBox}
import javafx.scene.text.Text
import javafx.scene.{Parent, Scene}
import javafx.stage.{Screen, Stage, WindowEvent}
import view.controllersutilities.FileUtilities

/**
  * Defines the menu
  */
trait Menu extends BorderPane {

  /**
    * the JavaFx stage
    *
    * @return the stage
    */
  def stage: Stage
}

/**
  * Object for menu
  */
object Menu {

  /**
    * Creates an instance of MenuImpl
    *
    * @param stage the stage created
    * @return the stage
    */
  def apply(stage: Stage): Menu = MenuImpl(stage)

  /**
    * Destroy the stage created
    *
    * @param m the menu
    * @return an Optional for destroying MenuImpl
    */
  def unapply(m: Menu): Option[Stage] = Some(m.stage)

  /**
    * New instance created
    *
    * @param stage the stage
    */
  private case class MenuImpl(var stage: Stage) extends Menu {

    private val scdcImage = new ImageView(
      getClass.getResource(ViewUtilities.TITLE_IMAGE_PATH).toString
    )
    private val showButton = new Button(ViewUtilities.BUTTON_SHOW_TEXT)
    private val settingsButton = new Button()
    private val deleteAllQueuesButton = new Button()
    private val selectScenarioText = new Text(
      ViewUtilities.SELECT_SCENARIO_TEXT
    )
    private val loadingText = new Text("")
    private val selectScenarioChoiceBox = new ChoiceBox[Scenario.Scenario]()
    private val choiceBoxOptions: ObservableList[Scenario.Scenario] =
      FXCollections.observableArrayList(Scenario.CROSS_ROAD, Scenario.SIDE_WALK)

    init()

    /**
      * Method to initialize scenario
      */
    def init(): Unit = {

      setPadding(new Insets(5))
      this.showButton.setDefaultButton(true)
      this.showButton.setOnAction((_: ActionEvent) => {
        this.showButton.setDisable(true)

        val p = new ProgressIndicator()
        val pHB = new HBox()
        pHB.getChildren.add(p)
        pHB.setAlignment(Pos.CENTER)
        setBottom(pHB)

        this.loadingText.setId(ViewUtilities.LOADING_TEXT_ID)

        var loadingArray: Array[String] = Array()
        this.selectScenarioChoiceBox.getValue match {
          case Scenario.CROSS_ROAD => loadingArray = ViewUtilities.loadingArrayCR
          case _                   => loadingArray = ViewUtilities.loadingArraySW
        }

        var i = 0
        import java.util.TimerTask
        class PrintLoadingText extends TimerTask {
          override def run(): Unit = {
            if (i < loadingArray.length) {
              loadingText.setText(
                ViewUtilities.LOADING_TEXT + loadingArray(i) + "..."
              )
            }
            i += 1
          }
        }

        val timer = new Timer
        timer.schedule(new PrintLoadingText, 0, 3000)

        val t =
          new Thread(() => showScenario(this.selectScenarioChoiceBox.getValue))
        t.setDaemon(true)
        t.start()
      })

      this.settingsButton.setId(ViewUtilities.SETTINGS_BUTTON_ID)

      this.settingsButton.setOnAction((_: ActionEvent) => {
        this.showSettings()
      })

      this.deleteAllQueuesButton
        .setId(ViewUtilities.DELETE_ALL_QUEUES_BUTTON_ID)

      this.deleteAllQueuesButton.setOnAction((_: ActionEvent) => {

        val alert = new Alert(
          AlertType.CONFIRMATION,
          ViewUtilities.DELETE_CONFIRMATION_TEXT,
          ButtonType.CANCEL,
          ButtonType.YES
        )
        alert.showAndWait()

        if (alert.getResult == ButtonType.YES) {
          FileUtilities.deleteAllQueues()
          FileUtilities.deleteCrossRoadConf()
          val alert = new Alert(
            AlertType.INFORMATION,
            ViewUtilities.SUCCESS_ELIMINATION_TEXT,
            ButtonType.CLOSE
          )
          alert.showAndWait()
        }

      })

      this.selectScenarioChoiceBox.setItems(this.choiceBoxOptions)
      this.selectScenarioChoiceBox.setValue(this.choiceBoxOptions.get(0))
      this.selectScenarioText.setId(ViewUtilities.SELECT_SCENARIO_TEXT_ID)

      val showButtonHB = new HBox()
      showButtonHB.getChildren.add(this.showButton)
      showButtonHB.setAlignment(Pos.CENTER)

      val settingsButtonHB = new HBox()
      settingsButtonHB.getChildren
        .addAll(this.settingsButton, this.deleteAllQueuesButton)
      settingsButtonHB.setAlignment(Pos.CENTER_LEFT)

      val borderPane = new BorderPane()
      borderPane.setCenter(showButtonHB)
      borderPane.setLeft(settingsButtonHB)

      val imageHB = new HBox()
      imageHB.getChildren.addAll(this.scdcImage, this.loadingText)
      imageHB.setAlignment(Pos.CENTER)

      val selectScenarioTextHB = new HBox()
      selectScenarioTextHB.getChildren.add(this.selectScenarioText)
      selectScenarioTextHB.setAlignment(Pos.CENTER_LEFT)

      val selectScenarioChoiceBoxHB = new HBox()
      selectScenarioChoiceBoxHB.getChildren
        .addAll(this.selectScenarioChoiceBox)
      selectScenarioChoiceBoxHB.setAlignment(Pos.CENTER_RIGHT)

      setBottom(borderPane)
      setTop(imageHB)
      setLeft(selectScenarioTextHB)
      setRight(selectScenarioChoiceBoxHB)
      setPrefSize(ViewUtilities.MENU_WIDTH, ViewUtilities.MENU_HEIGHT)
      stage.setOnCloseRequest((_: WindowEvent) => System.exit(0))
    }

    /**
      * Method to chose the appropriate scenario
      *
      * @param scenario the scenario to be shown
      */
    def showScenario(scenario: Scenario.Scenario): Unit = scenario match {
      case Scenario.CROSS_ROAD =>
        showScenario(
          ViewUtilities.CROSSROAD_FXML_PATH,
          ViewUtilities.CAR_ICON_PATH,
          ViewUtilities.CROSSROAD_SCENARIO_TITLE
        )
      case Scenario.SIDE_WALK =>
        showScenario(
          ViewUtilities.SIDEWAL_FXML_PATH,
          ViewUtilities.MAN_WALKING_ICON_PATH,
          ViewUtilities.SIDEWALK_SCENARIO_TITLE
        )
      case _ => new IllegalArgumentException
    }

    /**
      * Method to show program's settings
      *
      */
    def showSettings(): Unit = {
      val loader = new FXMLLoader(
        getClass.getResource(ViewUtilities.SETTINGS_FXML_PATH)
      )
      val root: Parent = loader.load()
      val scene = new Scene(root)
      scene.getStylesheets.add(ViewUtilities.SETTINGS_CSS_PATH)
      val primScreenBounds = Screen.getPrimary.getVisualBounds
      val newStage = new Stage()
      Platform.runLater(() => {
        newStage.getIcons.add(
          new Image(
            getClass.getResource(ViewUtilities.SETTINGS_ICON_PATH).toString
          )
        )
        newStage.setOnCloseRequest((_: WindowEvent) => newStage.close())
        newStage.setResizable(false)
        newStage.setTitle(ViewUtilities.CROSSROAD_SETTINGS_TITLE)
        newStage.setScene(scene)
        newStage.setX(
          (primScreenBounds.getWidth - ViewUtilities.SETTINGS_WIDTH) / 2
        )
        newStage.setY(
          (primScreenBounds.getHeight - ViewUtilities.SETTINGS_HEIGHT) / 2
        )
        newStage.show()
      })
    }

    /**
      * Method to create the chosen scenario
      *
      * @param loaderRes URL to the resource
      * @param imageRes  URL to image resource
      * @param title     title of the scenario
      */
    def showScenario(loaderRes: String,
                     imageRes: String,
                     title: String): Unit = {
      val loader = new FXMLLoader(getClass.getResource(loaderRes))
      val root: Parent = loader.load()
      val scene = new Scene(root)
      val primScreenBounds = Screen.getPrimary.getVisualBounds

      if (title == ViewUtilities.CROSSROAD_SCENARIO_TITLE) {
        scene.getStylesheets.add(ViewUtilities.CROSSROAD_CSS_PATH)
      } else {
        scene.getStylesheets.add(ViewUtilities.SIDEWALK_CSS_PATH)
      }

      Thread.sleep(ViewUtilities.LOADING_TIME)

      Platform.runLater(() => {
        stage.getIcons.add(new Image(getClass.getResource(imageRes).toString))
        stage.setOnCloseRequest((_: WindowEvent) => System.exit(0))
        stage.setResizable(false)
        stage.setTitle(title)
        stage.setScene(scene)
        stage.setX((primScreenBounds.getWidth - stage.getWidth) / 2)
        stage.setY((primScreenBounds.getHeight - stage.getHeight) / 2)
        stage.show()
      })

    }

  }

}
