package model.components.actuators

import model.components.basic.{Cell, DirectionEnum, LayerEnum}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SemaphoreTest extends FunSuite {

  val semaphore = Semaphore(
    Cell(4, 5),
    DirectionEnum.NORTH,
    SemaphoreInformation(ColorEnum.RED)
  )

  test("testSemaphorePosition") {
    assert(semaphore.position === Cell(4, 5))
  }

  test("testSemaphoreLayer") {
    assert(semaphore.layer === LayerEnum.SEMAPHORE_LAYER)
  }

  test("testSemaphoreState") {
    assert(semaphore.semaphoreInformation.state === ColorEnum.RED)
    semaphore.semaphoreInformation.state = ColorEnum.YELLOW
    assert(semaphore.semaphoreInformation.state === ColorEnum.YELLOW)
  }
}
