package model.components.actuators

import model.components.basic.{Cell, DirectionEnum}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StreetLampTest extends FunSuite {

  val streetLamp = StreetLamp(Cell(0, 0), DirectionEnum.WEST)

  test("testPosition") {
    assert(streetLamp.position === Cell(0, 0))
  }

  test("testState") {
    assert(streetLamp.state === StreetLampStates.LOW)
    streetLamp.state_(StreetLampStates.HIGH)
    assert(streetLamp.state === StreetLampStates.HIGH)
  }

}
