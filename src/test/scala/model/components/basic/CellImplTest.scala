package model.components.basic

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CellImplTest extends FunSuite {

  val cellImpl = Cell(0, 0)

  test("testGetX") {
    assert(cellImpl.row === 0)
  }

  test("testGetPosition") {
    assert(cellImpl.position === (0, 0))
  }

  test("testGetY") {
    assert(cellImpl.column === 0)
  }

}
