package model.components.basic

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ElementTest extends FunSuite {

  val c = Cell(1, 2)
  val element = Element(c, DirectionEnum.EAST, LayerEnum.STREET_LAYER)
  val street = Street(List(c, c), DirectionEnum.EAST)
  val vehicle = Vehicle(c, DirectionEnum.EAST)

  test("testList") {
    assert(element.position === c)
  }

  test("testAddList") {
    val list: List[Cell] = List(c, c, c)
    street.addCell(c)
    assert(street.cellList === list)
  }

  test("testDirection") {
    assert(
      element.direction === DirectionEnum.EAST ||
        element.direction === DirectionEnum.NORTH ||
        element.direction === DirectionEnum.WEST ||
        element.direction === DirectionEnum.SOUTH
    )
  }
  test("testSetDirection") {

    val dir = DirectionEnum.SOUTH
    element.direction_(dir)
    assert(element.direction === DirectionEnum.SOUTH)
  }
  test("testLayer") {
    assert(vehicle.layer === LayerEnum.VEHICLE_LAYER)
  }
}
