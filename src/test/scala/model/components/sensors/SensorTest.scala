package model.components.sensors

import model.components.basic.{Cell, DirectionEnum, Element, LayerEnum}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SensorTest extends FunSuite {

  val c = Cell(1, 2)
  val element = Element(c, DirectionEnum.EAST, LayerEnum.SENSOR_LAYER)
  val sensor = Sensor(c, DirectionEnum.EAST)

  test("testSensorStatus") {
    assert(sensor.isOn === false)
    sensor.state_(SensorEnum.ON)
    assert(sensor.isOn === true)
    assert(sensor.isDamaged === false)
    sensor.state_(SensorEnum.FAULT)
    assert(sensor.isDamaged === true)
  }
}
