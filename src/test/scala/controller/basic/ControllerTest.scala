package controller.basic

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import view.CrossRoadSettingsController
import view.controllersutilities.{CrossRoadSettings, FileUtilities}

@RunWith(classOf[JUnitRunner])
class ControllerTest extends FunSuite with BeforeAndAfterAll {

  var controller: Option[Controller] = None
  val crossRoadSettingsController = new CrossRoadSettingsController
  val crossRoadSettings: CrossRoadSettings =
    crossRoadSettingsController.getSettings
  val ELEMENTS_NUMBER
    : Int = crossRoadSettings.getEastVehicles + crossRoadSettings.getNorthVehicles + crossRoadSettings.getSouthVehicles + crossRoadSettings.getWestVehicles + 20

  override def beforeAll(): Unit = {
    FileUtilities.deleteAllQueues()
    controller = Some(
      Controller((8, 8), Scenario.CROSS_ROAD, Option(crossRoadSettings))
    )
  }

  override def afterAll(): Unit = FileUtilities.deleteAllQueues()

  test("ElementsInitialized") {
    assert(controller.get.elements.size === ELEMENTS_NUMBER)
  }

  test("NotifyObservers") {
    class Observer {
      def receiveUpdate(controller: Controller): Unit = {
        assert(controller.elements.size === ELEMENTS_NUMBER)
        controller.stopSimulation()
      }
    }
    val obs = new Observer
    controller.get.addObserver(obs.receiveUpdate)
    controller.get.notifyObservers()

  }

}
