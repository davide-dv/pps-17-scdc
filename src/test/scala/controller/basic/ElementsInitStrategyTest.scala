package controller.basic

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import view.CrossRoadSettingsController

@RunWith(classOf[JUnitRunner])
class ElementsInitStrategyTest extends FunSuite {

  test("CrossRoadInit") {
    val crossRoadSettingsController = new CrossRoadSettingsController
    val crossRoadSettings = crossRoadSettingsController.getSettings
    val ELEMENTS_NUMBER = crossRoadSettings.getEastVehicles + crossRoadSettings.getNorthVehicles + crossRoadSettings.getSouthVehicles + crossRoadSettings.getWestVehicles + 20
    val elements = ElementsInitStrategy(
      (8, 8),
      Scenario.CROSS_ROAD,
      Option(crossRoadSettings)
    ).init()
    assert(elements.size === ELEMENTS_NUMBER)
  }

}
