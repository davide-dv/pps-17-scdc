package controller.basic

import controller.actors.RegisterOperations
import model.components.basic.{Element, LayerEnum}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import view.CrossRoadSettingsController
import view.controllersutilities.FileUtilities

@RunWith(classOf[JUnitRunner])
class ElementActorsInitStrategyTest extends FunSuite with BeforeAndAfterAll {

  override def beforeAll(): Unit = FileUtilities.deleteAllQueues()

  override def afterAll(): Unit = FileUtilities.deleteAllQueues()

  test("ActorsInit") {
    val crossRoadSettingsController = new CrossRoadSettingsController
    val crossRoadSettings = crossRoadSettingsController.getSettings
    val ELEMENTS_NUMBER = crossRoadSettings.getEastVehicles + crossRoadSettings.getNorthVehicles + crossRoadSettings.getSouthVehicles + crossRoadSettings.getWestVehicles + 20
    val ACTORS_NUMBER = crossRoadSettings.getEastVehicles + crossRoadSettings.getNorthVehicles + crossRoadSettings.getSouthVehicles + crossRoadSettings.getWestVehicles + 16
    val elements = ElementsInitStrategy(
      (8, 8),
      Scenario.CROSS_ROAD,
      Option(crossRoadSettings)
    ).init()
    assert(elements.size === ELEMENTS_NUMBER)
    val actors = ElementActorsInitStrategy(
      elements
        .filter(e => e.layer != LayerEnum.STREET_LAYER)
        .map(e => e.asInstanceOf[Element]),
      Scenario.CROSS_ROAD
    ).init()
    assert(actors.size === ACTORS_NUMBER)
    actors.foreach(a => a.actorRegisterOperation(RegisterOperations.LEFT))
  }

}
