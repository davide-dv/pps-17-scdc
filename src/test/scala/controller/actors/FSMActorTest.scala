package controller.actors
import controller.actors.utilities.TopicEnum
import controller.basic.Scenario
import model.components.basic.{Cell, DirectionEnum, Vehicle}
import model.components.messages.{ElementInfo, GenericMessage}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import play.api.libs.json.{Json, OFormat}
import view.controllersutilities.FileUtilities

@RunWith(classOf[JUnitRunner])
class FSMActorTest extends FunSuite with BeforeAndAfterAll {

  implicit val elementInfoFormat: OFormat[ElementInfo] =
    Json.format[ElementInfo]
  implicit val cellMessageFormat: OFormat[Cell] = Json.format[Cell]
  implicit val genericMessageFormat: OFormat[GenericMessage] =
    Json.format[GenericMessage]

  var registerActor: Option[RegisterActor] = None

  var vehicle: Option[Vehicle] = None
  var fsm: Option[FSMActor] = None

  override def beforeAll(): Unit = {
    FileUtilities.deleteAllQueues()
    Thread.sleep(2000)

    registerActor = Some(RegisterActor(Scenario.CROSS_ROAD))
    registerActor.get.init()

    vehicle = Some(Vehicle(Cell(0, 0), DirectionEnum.NORTH))

    fsm = Some(FSMActor(vehicle.get, Scenario.CROSS_ROAD))
  }

  override def afterAll(): Unit = {
    FileUtilities.deleteAllQueues()
    Thread.sleep(2000)
  }

  test("testFSMActorInitAndSubscribe") {
    fsm.get.init()

    fsm.get.subscribe(List(TopicEnum.SCDC_VEHICLE_TOPIC))

    Thread.sleep(1000)

    assert(fsm.get.elements.size === 1)
  }

  test("testFSMActorWriteAndRead") {
    fsm.get.write(
      Json
        .toJson(
          GenericMessage(
            ElementInfo(vehicle.get.id, vehicle.get.direction),
            Cell(1, 0)
          )
        ),
      TopicEnum.SCDC_VEHICLE_TOPIC
    )

    Thread.sleep(1000)

    assert(
      fsm.get.elements
        .filter(e => e.id == vehicle.get.id)
        .head
        .position == Cell(1, 0)
    )

  }

  test("testFSMActorUtilities") {
    assert(
      fsm.get.actorUtility.get.msgList.head._1 == TopicEnum.SCDC_VEHICLE_TOPIC && fsm.get.actorUtility.get.msgList
        .get(TopicEnum.SCDC_VEHICLE_TOPIC)
        .size === 1
    )

    fsm.get.actorRegisterOperation(RegisterOperations.LEFT)
  }

}
