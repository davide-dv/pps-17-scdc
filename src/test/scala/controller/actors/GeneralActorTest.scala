package controller.actors
import controller.actors.utilities.TopicEnum
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import play.api.libs.json.Json
import view.controllersutilities.FileUtilities
@RunWith(classOf[JUnitRunner])
class GeneralActorTest extends FunSuite {

  test("testGeneralActor") {

    var generalActor = new GeneralActorImpl("id")
    var finalList: List[String] = List("test3", "test2", "test1", "test0")
    var list: List[String] = List()

    generalActor.subscribe(
      List(TopicEnum.SCDC_COMMAND_TOPIC),
      (x, y) => list = x.msg.as[String] :: list
    )

    var generalActor1 = new GeneralActorImpl("id1")

    Thread.sleep(1000)

    for (i <- 0 to 3) {
      generalActor1.publish(
        Json.toJson("test" + i),
        TopicEnum.SCDC_COMMAND_TOPIC
      )
      Thread.sleep(50)
    }

    Thread.sleep(1000)

    assert(list === finalList)

    generalActor.close()

    for (i <- 0 to 3) {
      generalActor1.publish(
        Json.toJson("test" + i),
        TopicEnum.SCDC_COMMAND_TOPIC
      )
      Thread.sleep(50)
    }

    assert(list === finalList || list.size === finalList.size)

    generalActor1.close()

    FileUtilities.deleteAllQueues()

    Thread.sleep(1000)

  }

}
