package controller.simulation.sensors
import model.components.basic._
import model.components.sensors.{Sensor, SensorEnum}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SensorBehaviourTest extends FunSuite {

  val c = Cell(1, 2)
  val element = Element(c, DirectionEnum.EAST, LayerEnum.SENSOR_LAYER)
  val sensor = Sensor(c, DirectionEnum.EAST)

  test("Triggered") {
    val vehicle = Vehicle(Cell(1, 2), DirectionEnum.EAST)
    import controller.actors.behaviours.sensors.SensorBehaviour._
    val sensor = Sensor(c, DirectionEnum.EAST)
    sensor.readValue(List(vehicle))
    assert(sensor.state === SensorEnum.ON)
  }

  test("Not triggered") {
    val vehicle =
      Vehicle(Cell(2, 2), DirectionEnum.EAST)
    import controller.actors.behaviours.sensors.SensorBehaviour._
    val sensor = Sensor(c, DirectionEnum.EAST)
    sensor.readValue(List(vehicle))
    assert(sensor.state === SensorEnum.OFF)
  }

}
