package controller.simulation.actuators

import controller.basic.{ElementsInitStrategy, Scenario}
import model.components.actuators.{ColorEnum, Semaphore, SemaphoreInformation}
import model.components.basic._
import model.components.sensors.{Sensor, SensorEnum}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import view.CrossRoadSettingsController
@RunWith(classOf[JUnitRunner])
class SemaphoreBehaviourTest extends FunSuite {

  val crossRoadSettingsController = new CrossRoadSettingsController
  private var elements: List[Element] =
    ElementsInitStrategy(
      (8, 8),
      Scenario.CROSS_ROAD,
      Option(crossRoadSettingsController.getSettings)
    ).init()
      .filter(e => !e.isInstanceOf[StreetElement])
      .asInstanceOf[List[Element]]

  test("testSetLongAwaitedSemaphore") {
    import controller.actors.behaviours.actuators.SemaphoreBehaviour._
    val eastSemaphore: Semaphore = this.elements
      .filter(
        el => el.direction == DirectionEnum.EAST && el.isInstanceOf[Semaphore]
      )
      .head
      .asInstanceOf[Semaphore]

    eastSemaphore.semaphoreInformation_(
      SemaphoreInformation(ColorEnum.RED, 0, 9999999999L)
    )

    eastSemaphore.setLongAwaitedSemaphore(
      elements
        .filter(e => e.isInstanceOf[Semaphore])
        .asInstanceOf[List[Semaphore]]
    )

    assert(eastSemaphore.longAwaitedSemaphore.get.equals(eastSemaphore))
  }

  test("testNextState") {

    import controller.actors.behaviours.actuators.SemaphoreBehaviour._
    val eastSemaphore: Semaphore = this.elements
      .filter(
        el => el.direction == DirectionEnum.EAST && el.isInstanceOf[Semaphore]
      )
      .head
      .asInstanceOf[Semaphore]

    eastSemaphore.nextState(elements)

    assert(eastSemaphore.semaphoreInformation.state === ColorEnum.GREEN)
  }

  test("updateWaitingCar") {
    import controller.actors.behaviours.actuators.SemaphoreBehaviour._
    val northSemaphore: Semaphore = this.elements
      .filter(
        el => el.direction == DirectionEnum.NORTH && el.isInstanceOf[Semaphore]
      )
      .head
      .asInstanceOf[Semaphore]

    val northSensor: Sensor = this.elements
      .filter(
        el => el.direction == DirectionEnum.NORTH && el.isInstanceOf[Sensor]
      )
      .head
      .asInstanceOf[Sensor]

    northSensor.state = SensorEnum.ON

    northSemaphore.updateWaitingCar(elements)

    assert(northSemaphore.semaphoreInformation.lastWaitingDetectedCar === 1)
  }

}
