package controller.simulation.actuators
import controller.basic.{ElementsInitStrategy, Scenario}
import model.components.actuators.{StreetLamp, StreetLampStates}
import model.components.basic._
import model.components.sensors.{Sensor, SensorEnum}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import view.CrossRoadSettingsController

@RunWith(classOf[JUnitRunner])
class StreetLampBehaviourTest extends FunSuite {

  test("testNextState") {

    val crossRoadSettingsController = new CrossRoadSettingsController
    val elements: List[Element] =
      ElementsInitStrategy(
        (8, 3),
        Scenario.SIDE_WALK,
        Option(crossRoadSettingsController.getSettings)
      ).init()
        .filter(e => !e.isInstanceOf[StreetElement])
        .asInstanceOf[List[Element]]

    val streetLamp: StreetLamp = elements
      .filter(e => e.position == Cell(0, 0) && e.isInstanceOf[StreetLamp])
      .head
      .asInstanceOf[StreetLamp]
    val sensor: Sensor = elements
      .filter(e => e.position == Cell(0, 1) && e.isInstanceOf[Sensor])
      .head
      .asInstanceOf[Sensor]

    sensor.state = SensorEnum.ON
    import controller.actors.behaviours.actuators.StreetLampBehaviour._
    streetLamp.nextState(elements)
    assert(streetLamp.state === StreetLampStates.HIGH)

    sensor.state = SensorEnum.OFF
    streetLamp.nextState(elements)
    assert(streetLamp.state === StreetLampStates.LOW)

    val sensor1: Sensor = elements
      .filter(e => e.position == Cell(1, 1) && e.isInstanceOf[Sensor])
      .head
      .asInstanceOf[Sensor]

    sensor1.state = SensorEnum.ON
    streetLamp.nextState(elements)
    assert(streetLamp.state === StreetLampStates.MEDIUM)
  }

}
