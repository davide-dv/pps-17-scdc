package controller.simulation.subjects

import model.components.actuators.{ColorEnum, Semaphore, SemaphoreInformation}
import model.components.basic.{Cell, DirectionEnum, Vehicle}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class VehicleBehaviourTest extends FunSuite {

  val c2 = Cell(1, 2)
  val vehicle = Vehicle(c2, DirectionEnum.EAST)
  val c3 = Cell(2, 2)
  val vehicle2 = Vehicle(c3, DirectionEnum.EAST)
  val sem = Semaphore(c2, DirectionEnum.EAST, SemaphoreInformation(ColorEnum.GREEN))

  test("checkFrontCarOrSemaphore") {
    import controller.actors.behaviours.subjects.VehicleBehaviour._
    vehicle.move(List(vehicle2, sem))
    assert(vehicle.position === Cell(1, 2))
    vehicle.move(List(sem))
    assert(vehicle.position === Cell(2, 2))
  }

  test("testDirection") {
    import controller.actors.behaviours.subjects.VehicleBehaviour._
    val vehicle3 = Vehicle(Cell(10, 3), DirectionEnum.WEST)
    val vehicle4 = Vehicle(Cell(10, 7), DirectionEnum.NORTH)
    val vehicle5 = Vehicle(Cell(1, 7), DirectionEnum.SOUTH)
    vehicle3.move(List(vehicle2, sem))
    vehicle4.move(List(vehicle2, sem))
    vehicle5.move(List(vehicle2, sem))
    assert(vehicle3.position === Cell(10 - 1, 3))
    assert(vehicle4.position === Cell(10, 7 - 1))
    assert(vehicle5.position === Cell(1, 7 + 2))
  }

}
