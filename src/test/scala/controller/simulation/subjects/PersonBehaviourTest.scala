package controller.simulation.subjects

import model.components.basic.{Cell, DirectionEnum, Person}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PersonBehaviourTest extends FunSuite {

  test("testMovePerson") {
    val personEast = Person(Cell(5, 1), DirectionEnum.EAST)
    val personWest = Person(Cell(5, 1), DirectionEnum.WEST)
    import controller.actors.behaviours.subjects.PersonBehaviour._
    personEast.move()
    assert(personEast.position === Cell(6, 1))
    personWest.move()
    assert(personWest.position === Cell(4, 1))
  }

}
