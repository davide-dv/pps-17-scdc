package projectutilities

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ObservableTest extends FunSuite {

  class Observed extends Observable[Observed] {
    var c: Int = 0

    def increment(): Unit = {
      c += 1
      notifyObservers()
    }
  }

  class Observer {
    var c: Int = 0
    def receiveUpdate(observed: Observed): Unit = {
      this.c = observed.c
    }
  }

  test("testNotifyObservers") {
    val observed = new Observed
    val observer = new Observer

    observed.addObserver(observer.receiveUpdate)
    assert(observed.c === 0)
    observed.increment()
    assert(observed.c === 1)
    Thread.sleep(1000)
    assert(observer.c === 1)
  }

}
