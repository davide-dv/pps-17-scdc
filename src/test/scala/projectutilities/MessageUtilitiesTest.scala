package projectutilities

import controller.basic.Scenario
import model.components.basic.{Cell, DirectionEnum, Vehicle}
import model.components.messages.{CrossRoadEnvironment, EnvironmentMessage, Environments}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MessageUtilitiesTest extends FunSuite {

  test("updateEnvironment") {
    val vehicle1 = Vehicle(Cell(0, 0), DirectionEnum.NORTH)
    val vehicle2 = Vehicle(Cell(1, 1), DirectionEnum.SOUTH)

    val elements = List(vehicle1, vehicle2)

    val ret = MessageUtilities.updateEnvironment(
      elements,
      EnvironmentMessage(
        Environments(
          crossRoadEnvironment =
            Option(CrossRoadEnvironment(List(), List(vehicle1), List()))
        )
      ),
      Scenario.CROSS_ROAD
    )

    assert(ret === List(vehicle1))

  }

}
